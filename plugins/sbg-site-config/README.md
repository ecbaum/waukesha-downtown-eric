# Compulse - Site Configuration Utility
## Description
This is a general purpose library that lets users fill out common site information into a single page without the need to use Advanced Custom Fields on the home page or create a Custom Post Type with site settings. This plugin also replaces the deprecated SBG Utils plugin. Some features of this plugin include:  

* Automatic updates to this plugin when pull requests are made to the BitBucket repository
* Shortcodes that allow class inputs
* More Social Media options
* Automatic Integration of FontAwesome
* Easy Access to Logo Inputs
* General Purpose Divi Builder configurations such as removal of the top header
* Divi Alt Tag fix
* And more features to come...

###Coming Soon
* Custom User-Defined Fields with Dynamic Shortcode generation

## Initial Setup

#### 1. Download WP Pusher Plugin if you haven't so already (Optional)
_If you already have wppusher.zip downloaded, you may skip this step._

[WP Pusher](https://wppusher.com/download "Click to download WP Pusher") [Click Link To Download]

#### 2. Remove Old Plugins
_This step is important, otherwise you will get a fatal error trying to activate the new plugin._

* Go to Plugins
* Look for __Divi Image Alt Text Fix__ and __SBG Utils__
* __Deactivate__ both and __Delete__ both when they are deactivated

#### 3. Install Downloaded Plugin
* Go to Plugins on the left menu on the Admin Dashboard inside the WordPress backend.
* Hit __Add New__
* Hit __Upload Plugin__
* Choose wppusher.zip and hit __Install Now__
* Once installed, press __Activate__
* If correctly installed you will see  
![logo](./images/wppusher.png "WP Pusher Icon")

#### 4. Configuring WP Pusher
__IMPORTANT__ - If you are user on BitBucket, please ensure you __LOG OFF__ before proceeding with this step. You will be using a read only credential to generate the necessary token.

##### If you are NOT logged into the Bitbucket account with solutions@compulse.com
* Go to the Bitbucket Tab  
![bitbucket](./images/bitbucket.png "Bitbucket Icon")  
* Hit __Obtain a Bitbucket token__
* You should see a field to login to BitBucket. Use the credentials below.
    * Username: solutions@compulse.com
    * Password: C#D^0U@o!nfzevgJpAJ%wqsLvS*ku6Gb
* Copy the generated token into the _Bitbucket token_ field.
* Hit __Save Bitbucket Token__
* At this point you will not need to log into the Bitbucket account again, when you get the token next time it will not ask for authentication.

##### If you ARE logged into the Bitbucket account with solutions@compulse.com
* Hit __Obtain a Bitbucket token__
* When you are logged in, you will skip the authentication process.
* Copy the generated token into the _Bitbucket token_ field.
* Hit __Save Bitbucket Token__

#### 5. Installing Updated SBG Site Config Plugin
* On the left sidebar in the WP Pusher menu, press __Install Plugin__
* Choose __Bitbucket__ as the __Repository Host__
* The plugin repository is  
~~~
sinclairdigitalsolutions/sbg-site-config
~~~
* Leave __Repository branch__ and __Repository subdirectory__ empty
* Make sure __Push-to-Deploy__ option is checked
* Hit __Install Plugin__
* Once Installed there will be a message at the top of the screen. Click the __activate__ link.
> Plugin was successfully installed. Go ahead and activate it.
* Once Activated you will be routed to the Plugins page. Look for __Compulse - Site Configuration__
* Hit __Manage__
* Inside you will see a box with the plugin title and a __Push-to_Deploy URL__ (See Example Below)
~~~
https://anguyen.wpengine.com/?wppusher-hook&token=6ed5e0e5d24418809c63520f929ab14f61968d078fee4402e0fa26e43035e6d8&package=c2JnLXNpdGUtY29uZmlnL3NiZy1zaXRlLWNvbmZpZy5waHA=
~~~
* Copy this link and upload it to the next available column in the Google Docs.
> https://docs.google.com/spreadsheets/d/1YmgAxmldSKZ18JKJxpaF3ecI2P_HCZyAt7vj183QkXI/edit?usp=sharing

#### 6. Checking SBG Site Config
_This step we are checking to ensure that the information of the old plugin gets pushed into the new plugin. This requires physically loading the Site Configuration Tool page at least once._

* Go to __Site Config__
* All the information from the previous plugin should automatically fill into the fields. If they are not filled, you will need to manually input the client information again.

#### 7. (Optional) Getting the WP Engine Install name
* Go to WP Engine on the left side of the site dashboard.  
![WP Engine](./images/wpengine.png "WP Engine Icon")
* In the third paragraph under __General Settings__ you will find a URL that says  
> anguyen.wpengine.com
* __anguyen__ is the Install Name, every site will be different.

#### 8. Complete

##### Contributors
+ Andy Nguyen
+ Kevin Hall
