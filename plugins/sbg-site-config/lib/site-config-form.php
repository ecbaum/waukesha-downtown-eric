<div style="margin-right: 50px;" class="core-config-form">
	<h2 style="text-align: center">Core Site Information</h2>
        <?php
            settings_fields('sbp-site-config');
            do_settings_fields('sbp-site-config', '');

            $companyName = $address = $city = $state = $zip = $phone = $phoneAlt = $fax = $locality = '';
            $siteName = get_option('sct-site-name');
            $email = get_option('sct-company-email');
            checkLegacyAttribute('sbp-company-name', 'sct-company-name', $companyName, true);
            checkLegacyAttribute('sbp-company-address', 'sct-company-address', $address, true);
            checkLegacyAttribute('sbp-company-city', 'sct-company-city', $city, true);
            checkLegacyAttribute('sbp-company-state', 'sct-company-state', $state, true);
            checkLegacyAttribute('sbp-company-zip', 'sct-company-zip', $zip, true);
            checkLegacyAttribute('sbp-company-phone', 'sct-company-phone', $phone, true);
            checkLegacyAttribute('sbp-company-phone2', 'sct-company-phone-alt', $phoneAlt, true);
            checkLegacyAttribute('sbp-company-fax', 'sct-company-fax', $fax, true);
            checkLegacyAttribute('sbp-footer-blurb', 'sct-company-locality', $locality, true);
        ?>

		<label>
			Site Name
			<input name="sct-site-name" value="<?php echo $siteName; ?>" type="text" />
		</label>

		<label>
			Company Name
			<input name="sct-company-name" value="<?php echo $companyName; ?>" type="text" />
		</label>

		<label>
			Company Street Address
			<input name="sct-company-address" value="<?php echo $address; ?>" type="text" />
		</label>

		<label>
			City
			<input name="sct-company-city" value="<?php echo $city; ?>" type="text" />
		</label>

		<label>
			State
			<input name="sct-company-state" value="<?php echo $state; ?>" type="text" />
		</label>

		<label>
			Zipcode
			<input name="sct-company-zip" value="<?php echo $zip; ?>" type="text" />
		</label>

        <label>
            Email Address
            <input name="sct-company-email" value="<?php echo $email; ?>" type="text" />
        </label>

		<label>
			Main Phone Number
			<input name="sct-company-phone" value="<?php echo $phone; ?>" type="text" />
		</label>

		<label>
			Alternate Phone Number
			<input name="sct-company-phone-alt" value="<?php echo $phoneAlt; ?>" type="text" />
		</label>

		<label>
			FAX Number
			<input name="sct-company-fax" value="<?php echo $fax; ?>" type="text" />
		</label>

		<label for="locality" class="locality-config">
			Locality Footer Text
		</label>
		<textarea name="sct-company-locality" id="locality" rows="10"><?php echo $locality; ?></textarea>
		<input type="submit" name="submit" id="submit" class="core-btn button" value="Save Settings" />
</div>

<style>
	.core-config-form {
        min-width: 550px;
		display: flex;
		flex-direction: column;
		text-align: right;
	}

	.core-config-form input[type=text] {
		margin-left: 10px;
        margin-bottom: 5px;
        min-width: 300px;
	}

	.locality-config {
		margin-top: 20px;
		text-align: left;
	}

	.locality-config span {
		float: right;
		font-size: 10px;
		color: #999;
	}

	.core-config-form textarea {
		resize: none;
	}

	.core-btn {
		margin: 10px 0 !important;
		display: block !important;
		border-radius: 0 !important;
		border-style: none !important;
		outline: none !important;
		box-shadow: none !important;
		background-color: #F95E1A !important;
		color: white !important;
		width: 170px !important;
		transition: background-color 150ms ease-in-out;
        position: fixed;
        bottom: 30px;
        right: 30px;
	}

	.core-btn:hover {
		background-color: #FB9E76 !important;
	}
</style>
