<div class="site-config-container">
    <div>
        <img class="site-config-logo" src="<?php echo plugins_url(); ?>/sbg-site-config/images/compulsediamond.png" />
    </div>

    <div>
        <h1 class="site-config-title">
            Site Configuration Tool
        </h1>

        <p class="site-config-sponsor">
            <small>By <a href="http://compulse.com" target="_blank">Compulse Integrated Marketing</a></small>
        </p>
    </div>
</div>

<style>
    .site-config-container {
        display: flex;
        flex-direction: row;
        align-items: center;
        margin: 30px 20px 0 20px;
    }

    .site-config-container div {

    }

    .site-config-logo {
        width: 60px;
        margin-right: 15px;
        animation: logo-animation infinite 5s linear;
    }

	.site-config-title {
		margin-bottom: 0;
        margin-top: 10px;
	}

	.site-config-sponsor {
		margin: 5px 0;
	}

    .site-config-sponsor a {
        text-decoration: none;
    }

    @keyframes logo-animation {
        from { transform: rotateY(0) translateZ(1px); }
        to {transform: rotateY(360deg) translateX(1px); }
    }
</style>