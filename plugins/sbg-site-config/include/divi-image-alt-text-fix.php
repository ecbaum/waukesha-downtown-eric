<?php

if ( !function_exists('cim_add_missing_divi_alt_text') ):


	function cim_add_missing_divi_alt_text($atts, $additional_atts, $slug, $address) {
		include_once "http-build-url.php";

		// Maps image url atts to the alt text att that goes with that image.
		$image_url_atts_to_alt_atts = [
			'src' => ['alt'],
			'image' => ['alt','image_alt'],
			'logo_image_url' => ['logo_alt_text']
		];

		foreach ($image_url_atts_to_alt_atts as $image_url_att => $alt_text_atts) {
			if ( array_key_exists($image_url_att, $atts) && !empty($atts[ $image_url_att ]) ) {
				$image_url = $atts[ $image_url_att ];

				if ( substr($image_url, 0, 1) == "/" ) {
					// Change to an absolute url.
					$image_url = site_url() . $image_url;
				}

				$site_url_parts = parse_url(site_url());

				$image_url_parts = parse_url($image_url);
				
				if ($image_url_parts["host"] != $site_url_parts["host"] && strpos($image_url_parts["host"], ".wpengine.com") !== FALSE) {
					// Change the image url's host to the site url's host.
					$image_url_parts["host"] = $site_url_parts["host"];
				
					$image_url = http_build_url($image_url_parts);
				}

				if ($image_url_parts["scheme"] != $site_url_parts["scheme"]) {
					$image_url_parts["scheme"] = $site_url_parts["scheme"];

					$image_url = http_build_url($image_url_parts);
				}

				$atts[ $image_url_att ] = $image_url;

				$empty_alt_atts = [];

				foreach ($alt_text_atts as $alt_att) {
					if ( array_key_exists($alt_att, $atts) && empty($atts[ $alt_att ]) ) {
						$empty_alt_atts[] = $alt_att;
					}
				}

				if ( !empty($empty_alt_atts) ) {
					$attachment_id = attachment_url_to_postid($image_url);

					if ($attachment_id > 0) {
						$attachment_alt_text = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);

						if ( !empty($attachment_alt_text) ) {
							foreach ($empty_alt_atts as $empty_alt_att) {
								$atts[ $empty_alt_att ] = $attachment_alt_text;
							}
						}
					}
				}
			}
		}

		return $atts;
	}


endif;

add_filter('et_pb_module_shortcode_attributes', 'cim_add_missing_divi_alt_text', 10, 4);












