<?php
/*
Plugin Name: Divi Map Extended Module
Plugin URI:  https://diviwebdesign.com/
Description: A Custom Map Module for Divi. Enhance the Google Map and impress your visitors with customized map design, custom pin icons and more!
Version:     3.1.2
Author:      Divi Web Design
Author URI:  https://diviwebdesign.com/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: dwd-map-extended
Domain Path: /languages
*/

require_once (dirname(__FILE__).'/includes/core/update-checker.php');
require_once (dirname(__FILE__).'/includes/core/dwd-core.php');
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
require (dirname(__FILE__).'/includes/core/persist-admin-notices-dismissal.php');
$licpath = base64_decode('ZG93bmxvYWRzL21vZHVsZXMv');

if ( ! function_exists( 'dme_initialize_extension' ) ):
/**
 * Creates the extension's main class instance.
 *
 * @since 1.0.0
 */
function dme_initialize_extension() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/DwdMapExtended.php';
}
add_action( 'divi_extensions_init', 'dme_initialize_extension' );
endif;

function dwd_map_scripts(){
    global $is_IE;
    if( $is_IE ) {
        wp_enqueue_script('dwd-mutationobserver-fix', plugin_dir_url( __FILE__ ) . 'assets/js/mutationobserver.min.js', array('divi-custom-script'), null, true);
    }
    wp_register_script('dwd-marker-clustering', plugin_dir_url( __FILE__ ) . 'assets/js/markerclusterer.js', array('google-maps-api'), null, true);
    wp_register_script('dwd-marker-with-label', plugin_dir_url( __FILE__ ) . 'assets/js/markerwithlabel.js', array('google-maps-api'), null, true);
    wp_register_script('dwd-infobox', plugin_dir_url( __FILE__ ) . 'assets/js/infobox.min.js', array('google-maps-api'), null, true);
    wp_register_script('dwd-nice-select', plugin_dir_url( __FILE__ ) . 'assets/js/jquery.nice-select.min.js', array('jquery'), null, true );
    wp_register_script('dwd-datatables', plugin_dir_url( __FILE__ ) . 'assets/js/datatables.min.js', array('jquery'), null, true );
    if ( et_core_is_fb_enabled() ) {
        wp_enqueue_script('dwd-infobox', plugin_dir_url( __FILE__ ) . 'assets/js/infobox.min.js', array('google-maps-api'), null, true);
        wp_enqueue_script('dwd-marker-with-label', plugin_dir_url( __FILE__ ) . 'assets/js/markerwithlabel.js', array('google-maps-api'), null, true);
        wp_enqueue_script('dwd-marker-clustering', plugin_dir_url( __FILE__ ) . 'assets/js/markerclusterer.js', array('google-maps-api'), null, true );
        wp_enqueue_script('dwd-nice-select', plugin_dir_url( __FILE__ ) . 'assets/js/jquery.nice-select.min.js', array('jquery'), null, true );
        wp_enqueue_script('dwd-datatables', plugin_dir_url( __FILE__ ) . 'assets/js/datatables.min.js', array('jquery'), null, true );
    }
}

add_action('wp_enqueue_scripts', 'dwd_map_scripts');

function dwd_map_admin() {
    wp_enqueue_style('dwd-map-admin', plugin_dir_url( __FILE__ ) . 'assets/css/dwd-map-extended-admin.css');
}

add_action('admin_enqueue_scripts', 'dwd_map_admin');

/*License Check*/
$lic = get_option( 'fhe_license_key' );
$lic_a = get_option( 'aio_license_key' );
$lic_m = get_option( 'map_license_key' );
$lic_fsa = get_option( 'fsa_license_key' );
$map_activated = get_option( 'map_license_key_activated' );
if ( $map_activated == 'activated' ) {
    $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
        'https://diviwebdesign.com/'.$licpath.'map.json',
        __FILE__,
        'dwd-maps-extended'
    );
}
if ( empty( $lic_m )) {
    function dwd_map_license_notice() {
        if ( ! MAPPAnD::is_admin_notice_active( 'map-disable-done-notice-forever' ) ) {
            return;
        }
        $class = 'notice notice-info is-dismissible';
        $message = __( 'Please enter your Map Extended Module plugin license key to get regular update and support by Navigating to Settings -> Divi Extended. You were given a license key when you purchased this item.', 'dwd-map' );

        printf( '<div data-dismissible="map-disable-done-notice-forever" class="%1$s"><p>%2$s</p></div>', $class, $message );
    }
    add_action( 'admin_notices', 'dwd_map_license_notice' );
}
add_action( 'admin_init', array( 'MAPPAnD', 'init' ) );


if ( ! function_exists( 'dwd_map_custom_post_type' ) ) :
function dwd_map_custom_post_type() {
     $labels = array(
        'name'                => _x( 'Divi Map Marker (Reserved for future update)', 'Post Type General Name', 'dwd-map-extended' ),
        'singular_name'       => _x( 'Divi Map', 'Post Type Singular Name', 'dwd-map-extended' ),
        'menu_name'           => __( 'Divi Map', 'dwd-map-extended' ),
        'parent_item_colon'   => __( 'Parent Marker', 'dwd-map-extended' ),
        'all_items'           => __( 'All Markers', 'dwd-map-extended' ),
        'view_item'           => __( 'View Marker', 'dwd-map-extended' ),
        'add_new_item'        => __( 'Add New Marker', 'dwd-map-extended' ),
        'add_new'             => __( 'Add New', 'dwd-map-extended' ),
        'edit_item'           => __( 'Edit Marker', 'dwd-map-extended' ),
        'update_item'         => __( 'Update Marker', 'dwd-map-extended' ),
        'search_items'        => __( 'Search Marker', 'dwd-map-extended' ),
        'not_found'           => __( 'Not Found', 'dwd-map-extended' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'dwd-map-extended' ),
    );
          
    $args = array(
        'description'         => __( 'Add Divi Map Extended Marker Category', 'dwd-map-extended' ),
        'labels'              => $labels,
        'public'             => false,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'can_export'         => true,
        'show_in_nav_menus'  => true,
        'query_var'          => true,
        'has_archive'        => true,
        'rewrite'            => array('slug' => 'map', 'with_front' => false),
        'capability_type'    => 'post',
        'hierarchical'       => false,
        'menu_position'      => 99,
        'menu_icon' => 'dashicons-location',
        'supports'           => array( 'title', 'author', 'editor', 'thumbnail', 'excerpt', 'comments', 'revisions', 'custom-fields' ),
        'capabilities' => array(
            'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
        ),
        'map_meta_cap' => true,
        'taxonomies'          => array( 'divi_map_category' ),
    );
     
    register_post_type( 'dwd_map_posttype', $args );
 
}
endif;
add_action( 'init', 'dwd_map_custom_post_type', 0 );

function dwd_map_custom_taxonomy() {

    $labels = array(
        'name'                       => _x( 'Divi Map Marker Categories', 'Taxonomy General Name', 'dwd-map-extended' ),
        'singular_name'              => _x( 'Divi Map Marker Category', 'Taxonomy Singular Name', 'dwd-map-extended' ),
        'menu_name'                  => __( 'Categories', 'dwd-map-extended' ),
        'all_items'                  => __( 'All Categories', 'dwd-map-extended' ),
        'parent_item'                => __( 'Parent Category', 'dwd-map-extended' ),
        'parent_item_colon'          => __( 'Parent Category:', 'dwd-map-extended' ),
        'new_item_name'              => __( 'New Item Name', 'dwd-map-extended' ),
        'add_new_item'               => __( 'Add New Marker Category', 'dwd-map-extended' ),
        'edit_item'                  => __( 'Edit Category', 'dwd-map-extended' ),
        'update_item'                => __( 'Update Category', 'dwd-map-extended' ),
        'view_item'                  => __( 'View Category', 'dwd-map-extended' ),
        'separate_items_with_commas' => __( 'Separate category with commas', 'dwd-map-extended' ),
        'add_or_remove_items'        => __( 'Add or remove categories', 'dwd-map-extended' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'dwd-map-extended' ),
        'popular_items'              => __( 'Popular Categories', 'dwd-map-extended' ),
        'search_items'               => __( 'Search Categories', 'dwd-map-extended' ),
        'not_found'                  => __( 'Not Found', 'dwd-map-extended' ),
        'no_terms'                   => __( 'No categories', 'dwd-map-extended' ),
        'items_list'                 => __( 'Marker Categories list', 'dwd-map-extended' ),
        'items_list_navigation'      => __( 'Marker Categories list navigation', 'dwd-map-extended' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'query_var'         => true,
    );
    register_taxonomy( 'divi_map_category', array( 'dwd_map_posttype' ), $args );

}
add_action( 'init', 'dwd_map_custom_taxonomy', 0 );