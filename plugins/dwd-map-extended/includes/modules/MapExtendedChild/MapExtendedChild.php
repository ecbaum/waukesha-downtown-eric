<?php

class ET_Builder_Module_Map_Extended_Item extends ET_Builder_Module {

	public $slug       = 'et_pb_map_pin_extended';
	// Module item has to use `child` as its type property
	public $type                     = 'child';
	// Module item's attribute that will be used for module item label on modal
	public $child_title_var          = 'title';
	// If the attribute defined on $this->child_title_var is empty, this attribute will be used instead
	public $child_title_fallback_var = 'subtitle';
	public $custom_css_tab              = false;

	// Full Visual Builder support
	public $vb_support = 'on';

	protected $module_credits = array(
		'module_uri' => 'https://diviwebdesign.com/',
		'author'     => 'Divi Web Design',
		'author_uri' => 'https://diviwebdesign.com/',
	);

	public function init() {
		$this->name = esc_html__( 'Pin', 'dwd-map-extended' );

		$this->advanced_setting_title_text = esc_html__( 'New Pin', 'dwd-map-extended' );
		// Module item's modal title
		$this->settings_text = esc_html__( 'Pin Settings', 'dwd-map-extended' );

		// Toggle settings
		$this->settings_modal_toggles = array(
			'general'  => array(
				'toggles' => array(
					'main_content' => esc_html__( 'Text', 'dwd-map-extended' ),
					'link' => esc_html__( 'Link', 'dwd-map-extended'),
					'image' => esc_html__( 'Image', 'dwd-map-extended'),
					'marker_filter' => esc_html__( 'Marker Filtering', 'dwd-map-extended'),
					'map'          => esc_html__( 'Map', 'dwd-map-extended' ),
				),
			),
			'advanced' => array(
                'toggles' => array(
                	'pin_color' => esc_html__( 'Pin Color Settings', 'dwd-map-extended' ),
                    'pin' => esc_html__( 'Custom Pin/Marker Icon', 'dwd-map-extended' ),
                ),
            ),
		);

		$this->advanced_fields = array(
			'text' => false,
			'box_shadow'            => array(
				'default' => false,
			),
			'borders'               => array(
				'default' => false,
			),
			'text_shadow'           => array(
				'default' => false,
			),
			'filters'                => false,
			'background'            => false,
			'fonts'                 => false,
			'max_width'             => false,
			'margin_padding' => false,
			'button'                => false,
			'link_options'          => false,
			'transform'          => false,
		);

	}

	public function get_fields() {
		return array(
			'title' => array(
				'label'           => esc_html__( 'Title', 'dwd-map-extended' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'The title will be used within the tab button for this tab.', 'dwd-map-extended' ),
				'toggle_slug'     => 'main_content',
			),
			'marker_label_text' => array(
                'label'           => esc_html__( 'Label Text', 'dwd-map-extended' ),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__( 'If you enable "Show Label on Markers" on the main setting, you can define the custom label text for the pin.', 'dwd-map-extended' ),
                'toggle_slug'     => 'main_content',
                //'show_if'   => array( 'parentModule:marker_label_all' => 'on')
            ),
            'pin_website_text' => array(
				'label'           => esc_html__( 'Website Text', 'dwd-map-extended' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'The website text will display under the content.', 'dwd-map-extended' ),
				'toggle_slug'     => 'main_content',
			),
            'pin_website_url' => array(
				'label'           => esc_html__( 'Website URL', 'dwd-map-extended' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'toggle_slug'     => 'main_content',
			),
			'pin_email_text' => array(
				'label'           => esc_html__( 'Email Text', 'dwd-map-extended' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'The Email text will display under the content.', 'dwd-map-extended' ),
				'toggle_slug'     => 'main_content',
			),
            'pin_email_url' => array(
				'label'           => esc_html__( 'Email URL', 'dwd-map-extended' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'toggle_slug'     => 'main_content',
			),
            'marker_filter_category_built_in' => array(
                'label'           => esc_html__( 'Select Category', 'dwd-map-extended' ),
                'type'            => 'categories',
                'option_category'  => 'basic_option',
				'taxonomy_name'   => 'divi_map_category',
                'description'     => esc_html__( 'This uses Divi Map Category. Please create a category before using Marker Filtering.', 'dwd-map-extended' ),
                'toggle_slug'     => 'marker_filter',
            ),
			'pin_address' => array(
				'label'             => esc_html__( 'Map Pin Address', 'dwd-map-extended' ),
				'type'              => 'text',
				'option_category'   => 'basic_option',
				'class'             => array( 'et_pb_pin_address' ),
				'description'       => esc_html__( 'Enter an address for this map pin, and the address will be geocoded and displayed on the map below.', 'dwd-map-extended' ),
				'additional_button' => sprintf(
					'<a href="#" class="et_pb_find_address button">%1$s</a>',
					esc_html__( 'Find', 'dwd-map-extended' )
				),
				'toggle_slug'       => 'map',
			),
			'zoom_level' => array(
				'type'    => 'hidden',
				'class'   => array( 'et_pb_zoom_level' ),
				'default' => '18',
				'default_on_front' => '',
				'option_category'  => 'basic_option',
			),
			'pin_address_lat' => array(
				'type'  => 'hidden',
				'class' => array( 'et_pb_pin_address_lat' ),
				'option_category' => 'basic_option',
			),
			'pin_address_lng' => array(
				'type'  => 'hidden',
				'class' => array( 'et_pb_pin_address_lng' ),
				'option_category' => 'basic_option',
			),
			'map_center_map' => array(
				'type'                  => 'center_map',
				'option_category'       => 'basic_option',
				'use_container_wrapper' => false,
				'toggle_slug'           => 'map',
			),
			'content' => array(
				'label'           => esc_html__( 'Content', 'dwd-map-extended' ),
				'type'            => 'tiny_mce',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Here you can define the content that will be placed within the infobox for the pin.', 'dwd-map-extended' ),
				'toggle_slug'     => 'main_content',
			),
			'marker_url' => array(
                'label'           => esc_html__( 'URL for Marker', 'dwd-map-extended' ),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__( 'This will open up a URL link on the marker depending the trigger method. Leave it blank if you do not want to open up a link.', 'dwd-map-extended' ),
                'toggle_slug'     => 'link',
            ),
            'marker_url_new_window' => array(
                'label'           => esc_html__( 'Url Opens', 'dwd-map-extended' ),
                'type'            => 'select',
                'option_category' => 'configuration',
                'options'         => array(
                    'off' => esc_html__( 'In The Same Window', 'dwd-map-extended' ),
                    'on'  => esc_html__( 'In The New Tab', 'dwd-map-extended' ),
                ),
                'default'           => 'off',
                'description' => esc_html__( 'Here you can choose whether or not your link opens in a new window', 'dwd-map-extended' ),
                'toggle_slug'     => 'link',
            ),
            'marker_url_trigger_method' => array(
                'label'           => esc_html__( 'Trigger Method', 'dwd-map-extended' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'options'         => array(
                    'click'     => esc_html__( 'On Click', 'dwd-map-extended' ),
                    'mouseover' => esc_html__( 'Hover', 'dwd-map-extended' ),
				),
				'default' => 'click',
                'default_on_front' => 'click',
                'description'         => esc_html__( 'Here you can choose the method of opening the URL link.', 'dwd-map-extended' ),
                'toggle_slug'     => 'link',
            ),
            'cover_image_src' => array(
                'label'              => esc_html__( 'Cover Image', 'dwd-map-extended' ),
                'type'               => 'upload',
                'option_category'    => 'basic_option',
                'upload_button_text' => esc_attr__( 'Upload a Cover Image', 'dwd-map-extended' ),
                'choose_text'        => esc_attr__( 'Choose a Cover Image', 'dwd-map-extended' ),
                'update_text'        => esc_attr__( 'Set As Cover Image', 'dwd-map-extended' ),
                'description'        => esc_html__( 'Upload your desired Cover Image.', 'dwd-map-extended' ),
                'toggle_slug'       => 'image',
            ),
            'pin_fill_color' => array(
                'label'             => esc_html__( 'Pin Fill Color', 'dwd-map-extended' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'default' => '#ea4335',
                'tab_slug' => 'advanced',
                'toggle_slug'       => 'pin_color',
				'description'       => esc_html__( 'Here you can define a custom fill color for the pin marker', 'dwd-map-extended' ),
				'hover'             => 'tabs',
                'show_if_not'         => array(
					'pin_on_off' => 'on',
				),
            ),
            'pin_stroke_color' => array(
                'label'             => esc_html__( 'Pin Stroke Color', 'dwd-map-extended' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'default' => '#ffffff',
                'tab_slug' => 'advanced',
                'toggle_slug'       => 'pin_color',
                'description'       => esc_html__( 'Here you can define a custom stroke color for the pin marker', 'dwd-map-extended' ),
				'hover'             => 'tabs',
				'show_if_not'         => array(
					'pin_on_off' => 'on',
				),
            ),
            'pin_circle_color' => array(
                'label'             => esc_html__( 'Pin Circle Color', 'dwd-map-extended' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'default' => '#811411',
                'tab_slug' => 'advanced',
                'toggle_slug'       => 'pin_color',
                'description'       => esc_html__( 'Here you can define a custom circle color for the pin marker', 'dwd-map-extended' ),
				'hover'             => 'tabs',
				'show_if_not'         => array(
					'pin_on_off' => 'on',
				),
            ),
			'pin_on_off' => array(
                'label'           => esc_html__( 'Use Custom Icon/Pin', 'dwd-map-extended' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'off' => esc_html__( 'No', 'dwd-map-extended' ),
                    'on'  => esc_html__( 'Yes', 'dwd-map-extended' ),
				),
				'default' => 'off',
				'default_on_front' => 'off',
                'tab_slug' => 'advanced',
                'toggle_slug'       => 'pin',
            ),
            'pin_src' => array(
                'label'              => esc_html__( 'Pin Icon URL', 'dwd-map-extended' ),
                'type'               => 'upload',
                'option_category'    => 'basic_option',
                'upload_button_text' => esc_attr__( 'Upload an Icon', 'dwd-map-extended' ),
                'choose_text'        => esc_attr__( 'Choose an Icon', 'dwd-map-extended' ),
                'update_text'        => esc_attr__( 'Set As Icon', 'dwd-map-extended' ),
                'description'        => esc_html__( 'Upload your desired Pin Icon, or type in the URL to the Pin Icon you would like to display.', 'dwd-map-extended' ),
                'tab_slug' => 'advanced',
                'toggle_slug'       => 'pin',
                'show_if'         => array(
					'pin_on_off' => 'on',
				),
            ),
            'pin_widthsize' => array(
                'label'           => esc_html__( 'Pin Icon Width (in PX)', 'dwd-map-extended' ),
                'type'              => 'range',
				'option_category'   => 'configuration',
				'default' => '40px',
				'default_on_front' => '40px',
				'default_unit'    => 'px',
				'range_settings'  => array(
						'min'  => '1',
						'max'  => '200',
						'step' => '1',
				),
                'tab_slug' => 'advanced',
                'toggle_slug'       => 'pin',
                'description'        => esc_html__( 'Enter your width value in numberic format eg 32.', 'dwd-map-extended' ),
                'show_if'         => array(
					'pin_on_off' => 'on',
				),
                //'mobile_options'  => true,
            ),
            'pin_heightsize' => array(
                'label'           => esc_html__( 'Pin Icon Height (in PX)', 'dwd-map-extended' ),
                'type'              => 'range',
				'option_category'   => 'configuration',
				'default' => '40px',
				'default_on_front' => '40px',
				'default_unit'    => 'px',
				'range_settings'  => array(
						'min'  => '1',
						'max'  => '200',
						'step' => '1',
				),
                'tab_slug' => 'advanced',
                'toggle_slug'       => 'pin',
                'description'        => esc_html__( 'Enter your height value in numberic format eg 32.', 'dwd-map-extended' ),
                'show_if'         => array(
					'pin_on_off' => 'on',
				),
                //'mobile_options'  => true,
			),
			'pin_change_src_on_off' => array(
                'label'           => esc_html__( 'Change Custom Icon/Pin on Click/Hover', 'dwd-map-extended' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'off' => esc_html__( 'No', 'dwd-map-extended' ),
                    'on'  => esc_html__( 'Yes', 'dwd-map-extended' ),
				),
				'default' => 'off',
				'default_on_front' => 'off',
                'tab_slug' => 'advanced',
				'toggle_slug'       => 'pin',
				'show_if'         => array(
					'pin_on_off' => 'on',
				),
            ),
            'pin_change_src' => array(
                'label'              => esc_html__( 'Pin Icon URL', 'dwd-map-extended' ),
                'type'               => 'upload',
                'option_category'    => 'basic_option',
                'upload_button_text' => esc_attr__( 'Upload an Icon', 'dwd-map-extended' ),
                'choose_text'        => esc_attr__( 'Choose an Icon', 'dwd-map-extended' ),
                'update_text'        => esc_attr__( 'Set As Icon', 'dwd-map-extended' ),
                'description'        => esc_html__( 'Upload your desired Pin Icon, or type in the URL to the Pin Icon you would like to display.', 'dwd-map-extended' ),
                'tab_slug' => 'advanced',
                'toggle_slug'       => 'pin',
                'show_if'         => array(
					'pin_on_off' => 'on',
					'pin_change_src_on_off' => 'on',
				),
			),
			'pin_change_widthsize' => array(
                'label'           => esc_html__( 'Pin Icon Width (in PX)', 'dwd-map-extended' ),
                'type'              => 'range',
				'option_category'   => 'configuration',
				'default' => '40px',
				'default_on_front' => '40px',
				'default_unit'    => 'px',
				'range_settings'  => array(
						'min'  => '1',
						'max'  => '200',
						'step' => '1',
				),
                'tab_slug' => 'advanced',
                'toggle_slug'       => 'pin',
                'description'        => esc_html__( 'Enter your width value in numberic format eg 32.', 'dwd-map-extended' ),
                'show_if'         => array(
					'pin_on_off' => 'on',
					'pin_change_src_on_off' => 'on',
				),
                //'mobile_options'  => true,
            ),
            'pin_change_heightsize' => array(
                'label'           => esc_html__( 'Pin Icon Height (in PX)', 'dwd-map-extended' ),
                'type'              => 'range',
				'option_category'   => 'configuration',
				'default' => '40px',
				'default_on_front' => '40px',
				'default_unit'    => 'px',
				'range_settings'  => array(
						'min'  => '1',
						'max'  => '200',
						'step' => '1',
				),
                'tab_slug' => 'advanced',
                'toggle_slug'       => 'pin',
                'description'        => esc_html__( 'Enter your height value in numberic format eg 32.', 'dwd-map-extended' ),
                'show_if'         => array(
					'pin_on_off' => 'on',
					'pin_change_src_on_off' => 'on',
				),
                //'mobile_options'  => true,
			),
		);
	}

	public function render( $attrs, $content = null, $render_slug ) {
		global $et_pb_tab_titles;
		global $et_pb_pin_classes;
        global $et_pb_pin_address_get;
        global $et_pb_pin_lat;
		global $et_pb_pin_lng;
		global $et_pb_content;
		global $et_pb_pin_on_off;
		global $et_pb_pin_src;
		global $et_pb_pin_widthsize;
		global $et_pb_pin_heightsize;
		global $et_pb_pin_fill_color;
		global $et_pb_pin_stroke_color;
		global $et_pb_pin_circle_color;
		global $et_pb_pin_fill_color_hover;
		global $et_pb_pin_stroke_color_hover;
		global $et_pb_pin_circle_color_hover;
		global $et_pb_pin_label_text;
		global $et_pb_pin_website_text;
		global $et_pb_pin_website_url;
		global $et_pb_pin_email_text;
		global $et_pb_pin_email_url;
		global $et_pb_pin_url;
		global $et_pb_pin_url_new_window;
		global $et_pb_pin_url_trigger_method;
        global $et_pb_marker_category;
		global $et_pb_marker_category_name;
		global $et_pb_marker_filter_category_built_in;
		global $et_pb_dwd_marker_filter_id_name;
		global $et_pb_pin_cover_image_src;
		global $et_pb_pin_change_src_on_off;
		global $et_pb_pin_change_src;
		global $et_pb_pin_change_widthsize;
		global $et_pb_pin_change_heightsize;

		$title = $this->props['title'];
		$pin_address_lat = $this->props['pin_address_lat'];
		$pin_address_lng = $this->props['pin_address_lng'];

		$pin_address_get = $this->props['pin_address'];
		$marker_label_text = $this->props['marker_label_text'];
		$pin_website_text = $this->props['pin_website_text'];
		$pin_website_url = $this->props['pin_website_url'];
		$pin_email_text = $this->props['pin_email_text'];
		$pin_email_url = $this->props['pin_email_url'];
		$marker_filter_category_built_in = $this->props['marker_filter_category_built_in'];
		$marker_url = $this->props['marker_url'];
		$marker_url_new_window = $this->props['marker_url_new_window'];
		$marker_url_trigger_method = $this->props['marker_url_trigger_method'];
		$cover_image_src = $this->props['cover_image_src'];
		$pin_fill_color = $this->props['pin_fill_color'];
		$pin_stroke_color = $this->props['pin_stroke_color'];
		$pin_circle_color = $this->props['pin_circle_color'];
		$pin_fill_color_hover = $this->get_hover_value( 'pin_fill_color' );
		$pin_stroke_color_hover = $this->get_hover_value( 'pin_stroke_color' );
		$pin_circle_color_hover = $this->get_hover_value( 'pin_circle_color' );
		$pin_on_off 	 = $this->props['pin_on_off'];
        $pin_src         = $this->props['pin_src'];
        $pin_widthsize   = $this->props['pin_widthsize'];
		$pin_heightsize  = $this->props['pin_heightsize'];
		$pin_change_src_on_off = $this->props['pin_change_src_on_off'];
		$pin_change_src = $this->props['pin_change_src'];
		$pin_change_widthsize   = $this->props['pin_change_widthsize'];
		$pin_change_heightsize  = $this->props['pin_change_heightsize'];

		$replace_htmlentities = array( '&#8221;' => '', '&#8243;' => '' );
		$title_html = htmlspecialchars( $title , ENT_QUOTES & ~ENT_COMPAT );

		$dwd_marker_filter_id_name = '';
		$dwd_marker_filter_id_category_name = '';
		/*
		if ( ! empty( $marker_filter_category ) ) {
			$args['tax_query'] = array(
	    		'taxonomy' => 'category',
	    		'field'    => 'id',
				'terms'    => $marker_filter_category,
				'operator' => 'IN'
			);

			$query = new WP_Query( $args );

			$marker_filter_id_name = ucfirst(get_cat_name( $marker_filter_category ));

			wp_reset_postdata();
		}*/

		if ( ! empty( $marker_filter_category_built_in ) ) {
			/*
			$term_slug = $marker_filter_category_built_in;
			$taxonomies = get_taxonomies();
			foreach ( $taxonomies as $tax_type_key => $taxonomy ) {
			    // If term object is returned, break out of loop. (Returns false if there's no object)
			    if ( $term_object = get_term_by( 'term_id', $term_slug , 'divi_map_category' ) ) {
			        break;
			    }
			}

			$term_id = '';
			$term_object == true ? $term_id = $term_object->name : $term_id = '';
			$dwd_marker_filter_id_name = ucfirst($term_id);
			*/
			$termsArray = array();
			$termsArrayList = array_map('intval', explode(',', $marker_filter_category_built_in));

			$terms = get_terms( array(
                'taxonomy' => 'divi_map_category',
                'include' => $termsArrayList,
                'hide_empty' => false,
            ));

		    foreach($terms as $term => $value) {
		    	if(!empty($terms)) {
		    		$termsArray[] = $value->name;
		    	} else {
		    		$termsArray[] = '';
		    	}
		    }

		    $arrayMapCategory = array();
		    foreach($termsArray as $value) {
		    	$arrayMapCategory[] = $value;
		    }

			$dwd_marker_filter_id_name = ucfirst(implode(", ", $termsArray));
			$dwd_marker_filter_id_category_name = ucfirst(implode(",", $arrayMapCategory));

		}

		if ( ! empty( $pin_address_lat ) ) {
			$pin_address_lat = strtr( $pin_address_lat, $replace_htmlentities );
		}
		if ( ! empty( $pin_address_lng ) ) {
			$pin_address_lng = strtr( $pin_address_lng, $replace_htmlentities );
		}

		//$et_pb_tab_titles[] = '' !== $title ? $title : esc_html__( 'Pin', 'dwd-map-extended' );
		$et_pb_tab_titles[] = $title;
		$et_pb_pin_classes[] = ET_Builder_Element::get_module_order_class( $render_slug );
		$et_pb_pin_address_get[] = $pin_address_get;
		$et_pb_pin_lat[] = $pin_address_lat;
		$et_pb_pin_lng[] = $pin_address_lng;
		$et_pb_content[] = $this->content;
		$et_pb_pin_on_off[] = $pin_on_off;
		$et_pb_pin_src[] = $pin_src;
		$et_pb_pin_widthsize[] = $pin_widthsize;
		$et_pb_pin_heightsize[] = $pin_heightsize;
		$et_pb_pin_fill_color[] = $pin_fill_color;
		$et_pb_pin_stroke_color[] = $pin_stroke_color;
		$et_pb_pin_circle_color[] = $pin_circle_color;
		$et_pb_pin_fill_color_hover[] = $pin_fill_color_hover;
		$et_pb_pin_stroke_color_hover[] = $pin_stroke_color_hover;
		$et_pb_pin_circle_color_hover[] = $pin_circle_color_hover;
		$et_pb_pin_label_text[] = $marker_label_text;
		$et_pb_pin_website_text[] = $pin_website_text;
		$et_pb_pin_website_url[] = $pin_website_url;
		$et_pb_pin_email_text[] = $pin_email_text;
		$et_pb_pin_email_url[] = $pin_email_url;
		$et_pb_pin_url[] = $marker_url;
		$et_pb_pin_url_new_window[] = $marker_url_new_window;
		$et_pb_pin_url_trigger_method[] = $marker_url_trigger_method;
		$et_pb_marker_category[] = $dwd_marker_filter_id_name;
		$et_pb_marker_category_name[] = $dwd_marker_filter_id_category_name;
		$et_pb_marker_filter_category_built_in[] = $marker_filter_category_built_in;
		$et_pb_pin_cover_image_src[] = $cover_image_src;
		$et_pb_pin_change_src_on_off[] = $pin_change_src_on_off;
		$et_pb_pin_change_src[] = $pin_change_src;
		$et_pb_pin_change_widthsize[] = $pin_change_widthsize;
		$et_pb_pin_change_heightsize[] = $pin_change_heightsize;

		if ( '' == $pin_website_text && '' == $pin_email_text ) {
		}

		$content = $this->content;

		$marker_listing_output = sprintf(
			'<div class="et_pb_map_pin_extended_address" data-lat="%1$s" data-lng="%2$s" data-marker-category="%6$s">
				<div class="dwd-marker-listing-cell dwd-marker-listing-title">%7$s%3$s</div>
				<div class="dwd-marker-listing-cell dwd-marker-category-listing">%6$s</div>
				<div class="dwd-marker-listing-cell dwd-marker-listing-address">%4$s</div>
				<div class="dwd-marker-listing-cell dwd-marker-listing-description">%5$s</div>
	      		<div class="dwd-marker-listing-cell dwd-marker-listing-direction">
		        	<span>Direction</span>
		        </div>
			</div>',
			esc_attr( $pin_address_lat ),
			esc_attr( $pin_address_lng ),
			esc_attr( $title ),
			esc_attr( $pin_address_get ),
			$content,
			( '' !== $marker_filter_category_built_in ? esc_attr( $dwd_marker_filter_id_name ) : '' ),
			( 'on' === $pin_on_off ? sprintf( '<div class="et_pb_main_blurb_image dwd-map-marker-listing-icon"><img src="%1$s" height="%3$s" width="%2$s"></div>',
				esc_url( $pin_src ),
				floatval(esc_attr( $pin_widthsize )),
				floatval(esc_attr( $pin_heightsize ))
			) :
				sprintf( '<div class="et_pb_main_blurb_image dwd-map-marker-listing-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22" height="50" viewBox="-16 -3 32 32"><path fill="%1$s" stroke="%2$s" stroke-width="1" d="M0,40 Q0,28 10,15 A15,15 0,1,0 -10,15 Q0,28 0,40"/><circle cx="0" cy="5" r="5" fill="%3$s" stroke="none"/></svg></div>',
					esc_attr( $pin_fill_color ),
					esc_attr( $pin_stroke_color ),
					esc_attr( $pin_circle_color )
				)
			)
		);

		// Remove automatically added classnames
		$this->remove_classname( array(
			'et_pb_module',
		) );

		return sprintf(
			'<div class="dwd_map_pin" data-lat="%1$s" data-lng="%2$s" data-title="%5$s" data-marker-filter-category="%13$s"%6$s%7$s%8$s%9$s%10$s%12$s%14$s>
				%3$s
				%4$s
			</div>
			%11$s',
			esc_attr( $pin_address_lat ),
			esc_attr( $pin_address_lng ),
			( '' != $title ? sprintf( '<h3>%1$s</h3>', esc_html( $title ) ) : '' ),
			( '' != $content ? sprintf( '<div class="infowindow">%1$s</div>', $content ) : '' ),
			$title_html,
			( 'on' === $pin_on_off ? sprintf( ' data-pin-custom-image="on" data-pin-src="%1$s" data-pin-width="%2$s" data-pin-height="%3$s"',
				esc_url( $pin_src ),
				floatval(esc_attr( $pin_widthsize )),
				floatval(esc_attr( $pin_heightsize ))
			) : '' ),
			esc_attr( " data-pin-fill-color={$pin_fill_color} data-pin-stroke-color={$pin_stroke_color} data-pin-circle-color={$pin_circle_color}" ),
			( '' !== $marker_label_text ? sprintf( ' data-marker-label-text="%1$s"', $marker_label_text ) : '' ),
			( '' != $pin_website_text || '' != $pin_email_text ? sprintf( ' data-pin-website-text="%1$s" data-pin-website-url="%2$s" data-pin-email-text="%3$s" data-pin-email="%4$s"',
				esc_attr( $pin_website_text ),
				esc_url( $pin_website_url ),
				esc_attr( $pin_email_text ),
				esc_attr( $pin_email_url )
			) : '' ),
			( '' === $pin_website_text && '' === $pin_email_text ? esc_attr( " data-pin-empty-links=on" ) : '' ),
			$marker_listing_output,
			( '' !== $marker_url ? esc_attr( " data-marker-url-link={$marker_url} data-marker-url-window={$marker_url_new_window} data-marker-url-trigger-method={$marker_url_trigger_method}" ) : '' ),
			//( '' !== $marker_filter_category || '' !== $marker_filter_category_built_in ? sprintf( ' data-marker-filter-category="%1$s"', $marker_filter_id_name ) : '' )
			( '' !== $marker_filter_category_built_in ? esc_attr( $dwd_marker_filter_id_name ) : '' ),
			( '' !== $cover_image_src ? sprintf( ' data-pin-cover-image="%1$s"',
				esc_url( $cover_image_src )
			) : '' )
		);
	}


	/*
	protected function _render_module_wrapper( $output = '', $render_slug = '' ) {
		$wrapper_settings    = $this->get_wrapper_settings( $render_slug );
		$slug                = $render_slug;
		$outer_wrapper_attrs = $wrapper_settings['attrs'];
		//$inner_wrapper_attrs = $wrapper_settings['inner_attrs'];

		/**
		 * Filters the HTML attributes for the module's outer wrapper. The dynamic portion of the
		 * filter name, '$slug', corresponds to the module's slug.
		 *
		 * @since 3.1
		 *
		 * @param string[]           $outer_wrapper_attrs
		 * @param ET_Builder_Element $module_instance
		 
		$outer_wrapper_attrs = apply_filters( "et_builder_module_{$slug}_outer_wrapper_attrs", $outer_wrapper_attrs, $this );

		return sprintf(
			'<div%1$s>
				%2$s
				%3$s
				%4$s
			</div>',
			et_html_attrs( $outer_wrapper_attrs ),
			$wrapper_settings['parallax_background'],
			$wrapper_settings['video_background'],
			$output
		);
	}
	*/
}

new ET_Builder_Module_Map_Extended_Item;
