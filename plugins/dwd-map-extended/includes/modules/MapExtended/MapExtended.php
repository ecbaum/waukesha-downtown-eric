<?php

class ET_Builder_Module_Map_Extended extends ET_Builder_Module {

	public $slug       = 'et_pb_map_extended';
	public $vb_support = 'on';
	// Module item's slug
	public $child_slug = 'et_pb_map_pin_extended';

	protected $module_credits = array(
		'module_uri' => 'https://diviwebdesign.com/',
		'author'     => 'Divi Web Design',
		'author_uri' => 'https://diviwebdesign.com/',
	);

	public function init() {
		$this->name = esc_html__( 'Map Extended', 'dme-dwd-map-extended' );
		// Toggle settings
		$this->settings_modal_toggles = array(
			'general'  => array(
				'toggles' => array(
					'map' => esc_html__( 'Map', 'et_builder' ),
				),
			),
			'advanced' => array(
				'toggles' => array(
					'controls' => esc_html__( 'Controls', 'et_builder' ),
					'marker_infowindow' => esc_html__( 'Marker & InfoWindow', 'et_builder' ),
					'infowindow' => esc_html__( 'InfoWindow Styles', 'et_builder' ),
					'styles'   => esc_html__( 'Map Styles', 'et_builder' ),
					'child_filters' => array(
						'title' => esc_html__( 'Map', 'et_builder' ),
						'priority' => 45,
					),
					'map_direction' => array(
						'title' => esc_html__( 'Map Direction', 'et_builder' ),
						'priority' => 60,
					),
					'marker_cluster' => array(
						'title' => esc_html__( 'Marker Clustering', 'et_builder' ),
						'tabbed_subtoggles' => true,
						'sub_toggles' => array(
							'general'     => array(
								'name' => 'General',
							),
							'hover'     => array(
								'name' => 'Hover',
							),
							'fonts'     => array(
								'name' => 'Fonts',
							),
						),
						'priority' => 60,
					),
					'marker_listing' => array(
						'title' => esc_html__( 'Marker Listing', 'et_builder' ),
						'tabbed_subtoggles' => true,
						'sub_toggles' => array(
							'general'     => array(
								'name' => 'General',
							),
							'thead'     => array(
								'name' => 'Thead',
							),
							'row'     => array(
								'name' => 'Row',
							),
						),
						'priority' => 60,
					),
					'marker_listing_pagination' => array(
						'title' => esc_html__( 'Marker Listing Pagination', 'et_builder' ),
						'tabbed_subtoggles' => true,
						'sub_toggles' => array(
							'general'     => array(
								'name' => 'General',
							),
							'current'     => array(
								'name' => 'Current',
							),
							'numbers'     => array(
								'name' => 'Numbers',
							),
						),
						'priority' => 60,
					),
				),
			),
		);
	}

	public function get_advanced_fields_config() {
		return array(
			'fonts' => array(
	          	'title' => array(
	              'label'    => esc_html__( 'Title', 'et_builder' ),
	              'css'      => array(
	                  'main' => "%%order_class%% .dwd-map-content h3",
				  ),
				  'font' => array(
					'default'      => 'Open Sans',
				),
				'font_weight' => array(
					'default'      => '600',
				  ),
	              'font_size' => array(
	                  'default'      => '22px',
	              ),
	              'text_color' => array(
					'default' => '#333',
					),
	              'line_height'    => array(
	                  'default'      => '1em',
	              ),
	              'hide_line_height'    => false,
	              'hide_text_color'     => false,
	              'hide_letter_spacing' => false,
	          ),
	          'content' => array(
	              'label'    => esc_html__( 'Content', 'et_builder' ),
	              'css'      => array(
	                  'main' => "%%order_class%% .dwd-map-content .infowindow",
				  ),
				  'font' => array(
					'default'      => 'Open Sans',
				  ),
				  'font_weight' => array(
					'default'      => '400',
				  ),
	              'font_size' => array(
	                  'default'      => '13px',
	              ),
	              'text_color' => array(
					'default' => '#666',
					),
	              'line_height'    => array(
	                  'default'      => '1.4em',
	              ),
	              'hide_line_height'    => false,
	              'hide_text_color'     => false,
	              'hide_letter_spacing' => false,
	          ),
	          'label' => array(
	              'label'    => esc_html__( 'Marker Label', 'et_builder' ),
	              'css'      => array(
	                  'main' => "%%order_class%% .dwd-map-label span",
	              ),
	              'font_size' => array(
	                  'default'      => '10px',
	              ),
	              'font' => array(
	                  'color' => '#fff',
	              ),
	              'line_height'    => array(
	                  'default'      => '1.1em',
	              ),
	              'hide_line_height'    => false,
	              'hide_text_color'     => false,
	              'hide_letter_spacing' => false,
	          ),
			  'map_pin_address_content_thead' => array(
	              'label'    => esc_html__( 'Thead', 'et_builder' ),
	              'css'      => array(
	                  'main' => "%%order_class%% .dwd-marker-listing-thead",
	              ),
	              'font_size' => array(
	                  'default'      => '14px',
	              ),
	              'font' => array(
	                  'color' => '#333',
	              ),
	              'line_height'    => array(
	                  'default'      => '1.7em',
	              ),
	              'hide_line_height'    => false,
	              'hide_text_color'     => false,
	              'hide_letter_spacing' => false,
					'toggle_slug'       => 'marker_listing',
					'sub_toggle'  => 'thead',
	          ),
	          'map_pin_address_content' => array(
	              'label'    => esc_html__( 'Row', 'et_builder' ),
	              'css'      => array(
	                  'main' => "%%order_class%% table.dwd_map_extended_child .et_pb_map_pin_extended",
	              ),
	              'font_size' => array(
	                  'default'      => '14px',
	              ),
	              'font' => array(
	                  'color' => '#333',
	              ),
	              'line_height'    => array(
	                  'default'      => '1.7em',
	              ),
	              'hide_line_height'    => false,
	              'hide_text_color'     => false,
	              'hide_letter_spacing' => false,
				  'toggle_slug'       => 'marker_listing',
				  'sub_toggle'  => 'row',
		          ),
	          	'marker_clustering_font' => array(
	              'label'    => esc_html__( 'Marker Cluster', 'et_builder' ),
	              'css'      => array(
	                  'main' => "%%order_class%% .dwd-markercluster",
	              ),
	              'font_size' => array(
	                  'default'      => '11px',
	              ),
	              'font' => array(
	                  'color' => '#fff',
	              ),
	              'line_height'    => array(
	                  'default'      => '42px',
	              ),
	              'hide_line_height'    => true,
	              'hide_text_color'     => false,
	              'hide_letter_spacing' => true,
	              'toggle_slug'     => 'marker_cluster',
	              'sub_toggle'  => 'fonts',
				 ),
				 'pagi_current_page_number' => array(
					'label'    => esc_html__( 'Pagination Current Page Number', 'et_builder' ),
					'css'      => array(
						'main' => "%%order_class%% .dataTables_wrapper .dataTables_paginate .paginate_button.current",
					),
					'font' => array(
					  'default'      => 'Open Sans',
					),
					'font_weight' => array(
					  'default'      => '400',
					),
					'font_size' => array(
						'default'      => '14px',
					),
					'text_color' => array(
					  'default' => '#333',
					  ),
					'line_height'    => array(
						'default'      => '1.7em',
					),
					'hide_line_height'    => false,
					'hide_text_color'     => false,
					'hide_letter_spacing' => false,
					'tab_slug'        => 'advanced',
					'toggle_slug'     => 'marker_listing_pagination',
					'sub_toggle'  => 'current',
				),
				'pagi_page_number' => array(
					'label'    => esc_html__( 'Pagination Page Number', 'et_builder' ),
					'css'      => array(
						'main' => "%%order_class%% .dataTables_wrapper .dataTables_paginate .paginate_button",
					),
					'font' => array(
					  'default'      => 'Open Sans',
					),
					'font_weight' => array(
					  'default'      => '400',
					),
					'font_size' => array(
						'default'      => '14px',
					),
					'text_color' => array(
					  'default' => '#333',
					  ),
					'line_height'    => array(
						'default'      => '1.7em',
					),
					'hide_line_height'    => false,
					'hide_text_color'     => false,
					'hide_letter_spacing' => false,
					'tab_slug'        => 'advanced',
					'toggle_slug'     => 'marker_listing_pagination',
					'sub_toggle'  => 'numbers',
				),
	        ),
	        /*
			'button'     => array(
				'button_one' => array(
					'label' => esc_html__( 'Marker Listing Button', 'et_builder' ),
					'css'      => array(
						'main' => "%%order_class%% .dwd-marker-listing-direction .et_pb_button",
					),
					'box_shadow' => array(
						'css' => array(
							'main' => "%%order_class%% .dwd-marker-listing-direction .et_pb_button",
						),
					),
				),
			),
			*/
			'box_shadow'            => array(
				'default' => array(
					'css' => array(
						'custom_style' => true,
					),
				),
				'infowindow'   => array(
					'label'               => esc_html__( 'InfoWindow Shadow', 'et_builder' ),
					'option_category'     => 'layout',
					'tab_slug'            => 'advanced',
					'toggle_slug'         => 'infowindow',
					'css'                 => array(
						'main' => '%%order_class%% .dwd-infowindow',
					),
					'show_if'         => array(
						'infowindow_styles' => 'custom_style',
					),
				),
			),
			'margin_padding' => array(
				'css' => array(
					'important' => array( 'custom_margin' ), // needed to overwrite last module margin-bottom styling
				),
			),
			'filters'               => array(
				'css' => array(
					'main' => '%%order_class%%',
				),
				'child_filters_target' => array(
					'tab_slug' => 'advanced',
					'toggle_slug' => 'child_filters',
				),
			),
			'child_filters'         => array(
				'css' => array(
					'main' => '%%order_class%% .gm-style>div>div>div>div>div>img',
				),
			),
		);
	}

	public function get_fields() {
		$dwd_animation_type_list = array(
			'fadeIn' => esc_html__( 'fadeIn', 'et_builder' ),
			'dwd-animation-off' => esc_html__( 'No Animation', 'et_builder' ),
			'bounce'    => esc_html__( 'bounce', 'et_builder' ),
			'flash'   => esc_html__( 'flash', 'et_builder' ),
			'pulse'     => esc_html__( 'pulse', 'et_builder' ),
			'rubberBand'  => esc_html__( 'rubberBand', 'et_builder' ),
			'shake' => esc_html__( 'shake', 'et_builder' ),
			'swing'    => esc_html__( 'swing', 'et_builder' ),
			'tada'   => esc_html__( 'tada', 'et_builder' ),
			'wobble'     => esc_html__( 'wobble', 'et_builder' ),
			'jello'     => esc_html__( 'jello', 'et_builder' ),
			'bounceIn'  => esc_html__( 'bounceIn', 'et_builder' ),
			'bounceInDown' => esc_html__( 'bounceInDown', 'et_builder' ),
			'bounceInLeft' => esc_html__( 'bounceInLeft', 'et_builder' ),
			'bounceInRight'    => esc_html__( 'bounceInRight', 'et_builder' ),
			'bounceInUp'   => esc_html__( 'bounceInUp', 'et_builder' ),
			'fadeInDown'     => esc_html__( 'fadeInDown', 'et_builder' ),
			'fadeInDownBig'  => esc_html__( 'fadeInDownBig', 'et_builder' ),
			'fadeInLeft' => esc_html__( 'fadeInLeft', 'et_builder' ),
			'fadeInLeftBig' => esc_html__( 'fadeInLeftBig', 'et_builder' ),
			'fadeInRight'    => esc_html__( 'fadeInRight', 'et_builder' ),
			'fadeInRightBig'   => esc_html__( 'fadeInRightBig', 'et_builder' ),
			'fadeInDown'     => esc_html__( 'fadeInDown', 'et_builder' ),
			'fadeInDownBig'  => esc_html__( 'fadeInDownBig', 'et_builder' ),
			'fadeInUp' => esc_html__( 'fadeInUp', 'et_builder' ),
			'fadeInUpBig' => esc_html__( 'fadeInUpBig', 'et_builder' ),
			'slideInUp' => esc_html__( 'slideInUp', 'et_builder' ),
			'slideInDown' => esc_html__( 'slideInDown', 'et_builder' ),
			'slideInLeft' => esc_html__( 'slideInLeft', 'et_builder' ),
			'slideInRight' => esc_html__( 'slideInRight', 'et_builder' ),
			'flip' => esc_html__( 'flip', 'et_builder' ),
			'flipInX' => esc_html__( 'flipInX', 'et_builder' ),
			'flipInY'    => esc_html__( 'flipInY', 'et_builder' ),
			'flipOutX' => esc_html__( 'flipOutX', 'et_builder' ),
			'flipOutY'    => esc_html__( 'flipOutY', 'et_builder' ),
			'rotateIn'   => esc_html__( 'rotateIn', 'et_builder' ),
			'rotateInDownLeft'     => esc_html__( 'rotateInDownLeft', 'et_builder' ),
			'rotateInDownRight'  => esc_html__( 'rotateInDownRight', 'et_builder' ),
			'rotateInUpLeft' => esc_html__( 'rotateInUpLeft', 'et_builder' ),
			'rotateInUpRight' => esc_html__( 'rotateInUpRight', 'et_builder' ),
			'zoomIn'    => esc_html__( 'zoomIn', 'et_builder' ),
			'zoomInDown'   => esc_html__( 'zoomInDown', 'et_builder' ),
			'zoomInLeft'     => esc_html__( 'zoomInLeft', 'et_builder' ),
			'zoomInRight'  => esc_html__( 'zoomInRight', 'et_builder' ),
			'zoomInUp' => esc_html__( 'zoomInUp', 'et_builder' ),
			'lightSpeedIn' => esc_html__( 'lightSpeedIn', 'et_builder' ),
			'lightSpeedOut' => esc_html__( 'lightSpeedOut', 'et_builder' ),
			'rollIn' => esc_html__( 'rollIn', 'et_builder' ),
			'rollOut' => esc_html__( 'rollOut', 'et_builder' ),
			'hinge' => esc_html__( 'hinge', 'et_builder' ),
		);
		return array(
			'google_maps_script_notice' => array(
				'type'              => 'warning',
				'value'             => et_pb_enqueue_google_maps_script(),
				'display_if'        => false,
				'message'           => esc_html__(
					sprintf(
						'The Google Maps API Script is currently disabled in the <a href="%s" target="_blank">Theme Options</a>. This module will not function properly without the Google Maps API.',
						admin_url( 'admin.php?page=et_divi_options' )
					),
					'et_builder'
				),
				'toggle_slug'       => 'map',
			),
			'google_api_key' => array(
				'label'             => esc_html__( 'Google API Key', 'et_builder' ),
				'type'              => 'text',
				'option_category'   => 'basic_option',
				'attributes'        => 'readonly',
				'additional_button' => sprintf(
					' <a href="%2$s" target="_blank" class="et_pb_update_google_key button" data-empty_text="%3$s">%1$s</a>',
					esc_html__( 'Change API Key', 'et_builder' ),
					esc_url( et_pb_get_options_page_link() ),
					esc_attr__( 'Add Your API Key', 'et_builder' )
				),
				'additional_button_type' => 'change_google_api_key',
				'class' => array( 'et_pb_google_api_key', 'et-pb-helper-field' ),
				'description'       => et_get_safe_localization( sprintf( __( 'The Maps module uses the Google Maps API and requires a valid Google API Key to function. Before using the map module, please make sure you have added your API key inside the Divi Theme Options panel. Learn more about how to create your Google API Key <a href="%1$s" target="_blank">here</a>.', 'et_builder' ), esc_url( 'http://www.elegantthemes.com/gallery/divi/documentation/map/#gmaps-api-key' ) ) ),
				'toggle_slug'       => 'map',
			),
			'address' => array(
				'label'             => esc_html__( 'Map Center Address', 'et_builder' ),
				'type'              => 'text',
				'option_category'   => 'basic_option',
				'additional_button' => sprintf(
					' <a href="#" class="et_pb_find_address button">%1$s</a>',
					esc_html__( 'Find', 'et_builder' )
				),
				'class' => array( 'et_pb_address' ),
				'description'       => esc_html__( 'Enter an address for the map center point, and the address will be geocoded and displayed on the map below.', 'et_builder' ),
				'toggle_slug'       => 'map',
			),
			'zoom_level' => array(
				'type'    => 'hidden',
				'class'   => array( 'et_pb_zoom_level' ),
				'default' => '18',
			),
			'address_lat' => array(
				'type'  => 'hidden',
				'class' => array( 'et_pb_address_lat' ),
			),
			'address_lng' => array(
				'type'  => 'hidden',
				'class' => array( 'et_pb_address_lng' ),
			),
			'map_center_map' => array(
				'type'                  => 'center_map',
				'use_container_wrapper' => false,
				'option_category'       => 'basic_option',
				'toggle_slug'           => 'map',
			),
			/*
			'mouse_wheel' => array(
				'label'           => esc_html__( 'Mouse Wheel Zoom', 'et_builder' ),
				'type'            => 'yes_no_button',
				'option_category' => 'configuration',
				'options' => array(
					'on'  => esc_html__( 'On', 'et_builder' ),
					'off' => esc_html__( 'Off', 'et_builder' ),
				),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'controls',
				'description'     => esc_html__( 'Here you can choose whether the zoom level will be controlled by mouse wheel or not.', 'et_builder' ),
				'default_on_front' => 'on',
			),
			*/
			'gesture_handling' => array(
                'label'           => esc_html__( 'Gesture Handing', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => 'auto',
                'options'         => array(
                    'none'               => esc_html__( 'None', 'et_builder' ),
                    'auto'               => esc_html__( 'Auto', 'et_builder' ),
                    'cooperative'        => esc_html__( 'Cooperative', 'et_builder' ),
                    'greedy'        => esc_html__( 'Greedy', 'et_builder' ),
                ),
            	'description'         => esc_html__( 'This will handle gestures on the map pan and zoom on page scroll. Read more at https://developers.google.com/maps/documentation/javascript/interaction', 'et_builder' ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
            ),
			'mobile_dragging' => array(
				'label'           => esc_html__( 'Draggable on Mobile', 'et_builder' ),
				'type'            => 'yes_no_button',
				'option_category' => 'configuration',
				'options'         => array(
					'on'  => esc_html__( 'On', 'et_builder' ),
					'off' => esc_html__( 'Off', 'et_builder' ),
				),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'controls',
				'description'     => esc_html__( 'Here you can choose whether or not the map will be draggable on mobile devices.', 'et_builder' ),
				'default_on_front' => 'on',
			),
			'use_grayscale_filter' => array(
				'label'           => esc_html__( 'Use Grayscale Filter', 'et_builder' ),
				'type'            => 'yes_no_button',
				'option_category' => 'configuration',
				'options'         => array(
					'off' => esc_html__( 'No', 'et_builder' ),
					'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'affects'     => array(
					'grayscale_filter_amount',
				),
				'tab_slug'    => 'advanced',
				'toggle_slug' => 'child_filters',
				'default_on_front' => 'off',
			),
			'grayscale_filter_amount' => array(
				'label'           => esc_html__( 'Grayscale Filter Amount (%)', 'et_builder' ),
				'type'            => 'range',
				'default'         => '0',
				'option_category' => 'configuration',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'child_filters',
				'depends_show_if' => 'on',
				'unitless'        => false,
			),
			//added
			'controls_ui' => array(
                'label'           => esc_html__( 'Show Controls', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'default' => 'on',
                'options' => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
                'description' => esc_html__( 'Here you can choose whether to show controls UI or not.', 'et_builder' ),
            ),
            'map_type_control_style' => array(
                'label'           => esc_html__( 'Control Style', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => 'default',
                'options'         => array(
                    'DEFAULT' => esc_html__( 'Default', 'et_builder' ),
                    'DROPDOWN_MENU' => esc_html__( 'Dropdown Menu', 'et_builder' ),
                    'HORIZONTAL_BAR' => esc_html__( 'Horizontal Bar', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
                'show_if'         => array(
					'controls_ui' => 'on',
				),
            ),
            'map_type_control_position' => array(
                'label'           => esc_html__( 'Control Position', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => 'TOP_LEFT',
                'options'         => array(
                    'TOP_LEFT' => esc_html__( 'Default (Top Left)', 'et_builder' ),
                    'TOP_CENTER' => esc_html__( 'Top Center', 'et_builder' ),
                    'TOP_RIGHT' => esc_html__( 'Top Right', 'et_builder' ),
                    'LEFT_CENTER' => esc_html__( 'Left Center', 'et_builder' ),
                    'RIGHT_CENTER' => esc_html__( 'Right Center', 'et_builder' ),
                    'BOTTOM_LEFT' => esc_html__( 'Bottom Left', 'et_builder' ),
                    'BOTTOM_CENTER' => esc_html__( 'Bottom Center', 'et_builder' ),
                    'BOTTOM_RIGHT' => esc_html__( 'Bottom Right', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
                'show_if'         => array(
					'controls_ui' => 'on',
				),
            ),
            'zoom_control_position' => array(
                'label'           => esc_html__( 'Zoom Control Position', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => 'LEFT_TOP',
                'options'         => array(
                    'LEFT_TOP' => esc_html__( 'Default (Left Top)', 'et_builder' ),
                    'LEFT_CENTER' => esc_html__( 'Left Center', 'et_builder' ),
                    'LEFT_BOTTOM' => esc_html__( 'Left Bottom', 'et_builder' ),
                    'RIGHT_TOP' => esc_html__( 'Right Top', 'et_builder' ),
                    'RIGHT_CENTER' => esc_html__( 'Right Center', 'et_builder' ),
                    'RIGHT_BOTTOM' => esc_html__( 'Right Bottom', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
                'show_if'         => array(
					'controls_ui' => 'on',
				),
            ),
            'streetview_control_position' => array(
                'label'           => esc_html__( 'StreetView Control Position', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => 'RIGHT_BOTTOM',
                'options'         => array(
                	'LEFT_TOP' => esc_html__( 'Left Top', 'et_builder' ),
                    'LEFT_CENTER' => esc_html__( 'Left Center', 'et_builder' ),
                    'LEFT_BOTTOM' => esc_html__( 'Left Bottom', 'et_builder' ),
                    'RIGHT_TOP' => esc_html__( 'Right Top', 'et_builder' ),
                    'RIGHT_CENTER' => esc_html__( 'Right Center', 'et_builder' ),
                    'RIGHT_BOTTOM' => esc_html__( 'Default (Right Bottom)', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
                'show_if'         => array(
					'controls_ui' => 'on',
				),
            ),
			'marker_center' => array(
                'label'           => esc_html__( 'Center Map', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'off' => esc_html__( 'No', 'et_builder' ),
                    'on'  => esc_html__( 'Yes', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
                'description' => esc_html__( 'If you have multiple markers, this will get all markers on the map and center them. Make use of the zoom level below to adjust to your liking.', 'et_builder' ),
            ),
            'marker_center_zoom' => array(
				'label'           => esc_html__( 'Center Map Zoom Level', 'et_builder' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'default' => '13',
				'default_on_front' => '13',
				'range_settings'  => array(
					'min'  => '3',
					'max'  => '18',
					'step' => '1',
				),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'controls',
                'description'     => esc_html__( 'The zoom level of the map when centering the map.', 'et_builder' ),
                'show_if'         => array(
					'marker_center' => 'on',
				),
			),
			'infowindow_styles' => array(
                'label'           => esc_html__( 'InfoWindow Styles', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => 'default',
                'options'         => array(
                    'default' => esc_html__( 'Google (Default)', 'et_builder' ),
                    'custom_style' => esc_html__( 'Custom Styling', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'infowindow',
            ),
            'infowindow_radius' => array(
				'label'           => esc_html__( 'Border Radius', 'et_builder' ),
				'type'            => 'range',
				'option_category' => 'layout',
				'tab_slug'        => 'advanced',
                'toggle_slug'     => 'infowindow',
				'default_unit'    => 'px',
				'default'=> '3px',
				'default_on_front'=> '3px',
				'range_settings' => array(
					'min'  => '0',
					'max'  => '30',
					'step' => '1',
				),
				'show_if'         => array(
					'infowindow_styles' => 'custom_style',
				),
			),
			'infowindow_close_icon' => array(
				'label'               => esc_html__( 'Close Icon', 'et_builder' ),
				'type'                => 'select_icon',
				'option_category'     => 'basic_option',
				'default' => '',
				'class'               => array( 'et-pb-font-icon' ),
				'tab_slug'        => 'advanced',
                'toggle_slug'     => 'infowindow',
                'show_if'         => array(
					'infowindow_styles' => 'custom_style',
				),
			),
			'infowindow_close_color' => array(
                'label'             => esc_html__( 'Close Button Color', 'et_builder' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'infowindow',
                'show_if'         => array(
					'infowindow_styles' => 'custom_style',
				),
            ),
			'infowindow_bg_color' => array(
                'label'             => esc_html__( 'Background Color', 'et_builder' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'infowindow',
                'show_if'         => array(
					'infowindow_styles' => 'custom_style',
				),
            ),
            'infowindow_animation_in' => array(
				'label'             => esc_html__( 'Open Marker Animation', 'et_builder' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'default'           => 'dwd-animation-off',
				'default_on_front' => 'dwd-animation-off',
				'options'         => $dwd_animation_type_list,
				'tab_slug'        => 'advanced',
                'toggle_slug'     => 'infowindow',
                'show_if'         => array(
					'infowindow_styles' => 'custom_style',
				),
			),
			'info_window_width' => array(
				'label'           => esc_html__( 'InfoWindow Width', 'et_builder' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'default' => '200',
				'default_on_front' => '200',
				'default_unit' => '',
				'range_settings'  => array(
					'min'  => '180',
					'max'  => '400',
					'step' => '1',
				),
				'tab_slug'        => 'advanced',
                'toggle_slug'     => 'marker_infowindow',
			),
			'info_window' => array(
                'label'           => esc_html__( 'Show InfoWindow on load', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'default' => 'off',
				'default_on_front' => 'off',
                'options'         => array(
                    'off' => esc_html__( 'No', 'et_builder' ),
                    'on'  => esc_html__( 'Yes', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'marker_infowindow',
            ),
			'info_window_method' => array(
                'label'           => esc_html__( 'InfoWindow Trigger Method', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => 'click',
                'default_on_front' => 'click',
                'options'         => array(
                    'click'               => esc_html__( 'On Click', 'et_builder' ),
                    'mouseover'               => esc_html__( 'Hover', 'et_builder' ),
                ),
                'description'         => esc_html__( 'By default, Google Map will display info window when you click on the pin marker, however you can have mousehover/hover to display info window as well.', 'et_builder' ),
                'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_infowindow',
            ),
            'bounce_on_click_on_off' => array(
                'label'           => esc_html__( 'Bounce Animation on Marker Pin', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'off' => esc_html__( 'No', 'et_builder' ),
                    'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'default_on_front' => 'off',
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'marker_infowindow',
                'description'         => esc_html__( 'Bounce Animation will based on the InfoWindow Trigger Method Above.', 'et_builder' ),
            ),
            'marker_animation' => array(
                'label'           => esc_html__( 'Drop Animation on Marker Pin On Load', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'off' => esc_html__( 'No', 'et_builder' ),
                    'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'default_on_front' => 'off',
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'marker_infowindow',
            ),
            'marker_auto_center' => array(
                'label'           => esc_html__( 'Auto Center Map on Marker Click/Hover', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'default' => 'off',
				'default_on_front' => 'off',
                'options'         => array(
                    'off' => esc_html__( 'No', 'et_builder' ),
                    'on'  => esc_html__( 'Yes', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'marker_infowindow',
                'description'         => esc_html__( 'This will auto center the map on marker click/hover depending on the InfoWindow Trigger Method Above.', 'et_builder' ),
            ),
            'marker_label_all' => array(
                'label'           => esc_html__( 'Show Label on Markers', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default' => 'off',
                'default_on_front' => 'off',
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'marker_infowindow',
                'description'     => esc_html__( 'Here you choose to show a small label under all markers. By default, the label will show the title of the pin. If you wish to have custom text, you will need to go pin settings to change them.', 'et_builder' ),
            ),
            'marker_label_bg' => array(
                'label'             => esc_html__( 'Background Color', 'et_builder' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'default' => 'rgba(80, 80, 80, 0.9)',
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'label',
                'show_if'         => array(
					'marker_label_all' => 'on',
				),
            ),
            'marker_label_radius' => array(
                'label'             => esc_html__( 'Border Radius', 'et_builder' ),
                'type'              => 'range',
                'option_category'   => 'configuration',
                'default'           => '4px',
                'range_settings'  => array(
                        'min'  => '0',
                        'max'  => '20',
                        'step' => '1',
                ),
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'label',
                'show_if'         => array(
					'marker_label_all' => 'on',
				),
            ),
            'marker_label_padding' => array(
				'label'           => esc_html__( 'Padding', 'et_builder' ),
				'type'      => 'custom_padding',
                'option_category'   => 'layout',
                'tab_slug'        => 'advanced',
				'toggle_slug'       => 'label',
                'sides'     => array('top','right','bottom','left'),
                'mobile_options'    => true,
                'responsive'      => true,
				'show_if'         => array(
					'marker_label_all' => 'on',
				),
			),
			'show_marker_listing' => array(
                'label'           => esc_html__( 'Show Marker Listing', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default'         => 'off',
				'tab_slug'        => 'advanced',
	            'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'general',
                'description'     => esc_html__( 'This will display your markers information either on the top or the bottom of the map. Clicking on relevant marker will show up on the map.', 'et_builder' ),
            ),
            'show_marker_listing_icon' => array(
                'label'           => esc_html__( 'Show Marker Icon', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default'         => 'off',
				'tab_slug'        => 'advanced',
	            'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'general',
                'description'     => esc_html__( 'This will display your markers icon before the title.', 'et_builder' ),
            ),
            'map_marker_listing_zoom' => array(
				'label'           => esc_html__( 'Zoom Level', 'et_builder' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'default' => '19',
				'default_on_front' => '19',
				'range_settings'  => array(
					'min'  => '9',
					'max'  => '40',
					'step' => '1',
				),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'general',
                'description'     => esc_html__( 'The zoom level of the map when clicking on the address.', 'et_builder' ),
                'show_if'         => array(
					'show_marker_listing' => 'on',
				),
			),
			/*
			'show_marker_listing_title' => array(
                'label'           => esc_html__( 'Show Marker Listing Title', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default'         => 'on',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
            ),*/
			'marker_listing_title' => array(
				'label'           => esc_html__( 'Thead Title Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'default' => 'Title',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
			),
			'show_marker_listing_address' => array(
                'label'           => esc_html__( 'Show Marker Listing Address', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default'         => 'on',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
            ),
			'marker_listing_address' => array(
				'label'           => esc_html__( 'Thead Address Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'default' => 'Address',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
					'show_marker_listing_address' => 'on',
				),
			),
			'show_marker_listing_description' => array(
                'label'           => esc_html__( 'Show Marker Listing Description', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default'         => 'on',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
            ),
			'marker_listing_description' => array(
				'label'           => esc_html__( 'Thead Description Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'default' => 'Description',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
					'show_marker_listing_description' => 'on',
				),
			),
			'show_marker_listing_category' => array(
                'label'           => esc_html__( 'Show Marker Listing Category', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default'         => 'on',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
					'marker_filtering' => 'on',
				),
            ),
			'marker_listing_category' => array(
				'label'           => esc_html__( 'Thead Category Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'default' => 'Categories',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
					'marker_filtering' => 'on',
					'show_marker_listing_category' => 'on',
				),
			),
			'show_marker_listing_direction' => array(
                'label'           => esc_html__( 'Show Marker Listing Direction', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options'         => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default'         => 'on',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
					'map_directions' => 'on',
				),
            ),
			'marker_listing_direction' => array(
				'label'           => esc_html__( 'Thead Direction Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'default' => 'Directions',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
					'map_directions' => 'on',
					'show_marker_listing_direction' => 'on',
				),
			),
			'marker_listing_bg_thead' => array(
	            'label'             => esc_html__( 'Background Color', 'et_builder' ),
	            'type'              => 'color-alpha',
	            'custom_color'      => true,
				'default' => '#f9fafb',
	            'tab_slug'        => 'advanced',
	            'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'thead',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
			),
			'marker_listing_odd_bg' => array(
		        'label'             => esc_html__( 'Odd Background Color', 'et_builder' ),
		        'type'              => 'color-alpha',
				'custom_color'      => true,
				'default' => '#ffffff',
		        'tab_slug'        => 'advanced',
		        'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'row',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
	        ),
			'marker_listing_bg' => array(
		        'label'             => esc_html__( 'Even Background Color', 'et_builder' ),
		        'type'              => 'color-alpha',
				'custom_color'      => true,
				'default' => 'rgba(0,0,50,.02)',
		        'tab_slug'        => 'advanced',
		        'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'row',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
			),
			'marker_listing_bg_hover' => array(
		        'label'             => esc_html__( 'Background Hover Color', 'et_builder' ),
		        'type'              => 'color-alpha',
		        'custom_color'      => true,
		        'tab_slug'        => 'advanced',
				'toggle_slug'     => 'marker_listing',
				'default' => '#f4f4f4',
				'sub_toggle'  => 'row',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
	        ),
	        'marker_listing_border_color' => array(
	            'label'             => esc_html__( 'Border Color', 'et_builder' ),
	            'type'              => 'color-alpha',
				'custom_color'      => true,
				'default' => 'rgba(34,36,38,.15)',
	            'depends_show_if'   => 'on',
	            'tab_slug'        => 'advanced',
	            'toggle_slug'     => 'marker_listing',
				'sub_toggle'  => 'row',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
			),
			'listing_pagi_border' => array(
	            'label'             => esc_html__( 'Border Color', 'et_builder' ),
	            'type'              => 'color-alpha',
				'custom_color'      => true,
				'default' => 'rgba(34,36,38,.15)',
	            'tab_slug'        => 'advanced',
		        'toggle_slug'     => 'marker_listing_pagination',
				'sub_toggle'  => 'general',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
			),
			'listing_pagi_current_background' => array(
		        'label'             => esc_html__( 'Background Color', 'et_builder' ),
		        'type'              => 'color-alpha',
		        'custom_color'      => true,
		        'tab_slug'        => 'advanced',
		        'toggle_slug'     => 'marker_listing_pagination',
				'sub_toggle'  => 'current',
				'default' => '#eee',
				'default_on_front' => '#eee',
				'hover'             => 'tabs',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
			),
			'listing_pagi_page_num_background' => array(
		        'label'             => esc_html__( 'Background Color', 'et_builder' ),
		        'type'              => 'color-alpha',
		        'custom_color'      => true,
		        'tab_slug'        => 'advanced',
		        'toggle_slug'     => 'marker_listing_pagination',
				'sub_toggle'  => 'numbers',
				'hover'             => 'tabs',
				'show_if'         => array(
					'show_marker_listing' => 'on',
				),
	        ),
			'map_directions' => array(
                'label'           => esc_html__( 'Show Directions', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options' => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
				'default' => 'off',
				'default_on_front' => 'off',
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
                'description' => esc_html__( 'Calculate directions using multiple transportation modes & multi-part directions with turn-by-turn, walking, cycling & public transit routing.', 'et_builder' ),
            ),
            'map_directions_placement' => array(
                'label'           => esc_html__( 'Map Direction Placement', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => 'inside',
                'options'         => array(
                    'inside'               => esc_html__( 'Inside the Map', 'et_builder' ),
                    'bottom'               => esc_html__( 'Below the Map', 'et_builder' ),
                ),
            	'description'         => esc_html__( 'Google Map direction placement.', 'et_builder' ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
                'show_if'         => array(
					'map_directions' => 'on',
				),
            ),
			'map_directions_text' => array(
                  'label'           => esc_html__( 'Show Text Direction', 'et_builder' ),
                  'type'            => 'yes_no_button',
                  'option_category' => 'configuration',
                  'options' => array(
                      'on'  => esc_html__( 'On', 'et_builder' ),
                      'off' => esc_html__( 'Off', 'et_builder' ),
                  ),
                  'default' => 'off',
                  'tab_slug'        => 'advanced',
                  'toggle_slug'     => 'controls',
				  'show_if'         => array(
					'map_directions' => 'on',
				  ),
            ),
            'map_direction_current_location_text' => array(
				'label'           => esc_html__( 'Current Location Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'default' => 'Current Location',
				'tab_slug'        => 'advanced',
				'toggle_slug'         => 'map_direction',
				'show_if'         => array(
					'map_directions' => 'on',
				),
			),
			'map_direction_location_icon' => array(
				'label'               => esc_html__( 'Location Icon', 'et_builder' ),
				'type'                => 'select_icon',
				'option_category'     => 'basic_option',
				'default' => '',
				'class'               => array( 'et-pb-font-icon' ),
				'tab_slug'        => 'advanced',
				'toggle_slug'         => 'map_direction',
				'description'         => esc_html__( 'Choose an icon to display for your location button.', 'et_builder' ),
				'show_if'         => array(
					'map_directions' => 'on',
				),
			),
			/*
			'map_direction_location_icon_size' => array(
				'label'           => esc_html__( 'Icon Font Size', 'et_builder' ),
				'type'            => 'range',
				'option_category' => 'font_option',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'map_direction',
				'default'         => '14px',
				'default_unit'    => 'px',
				'default_on_front'=> '',
				'range_settings' => array(
					'min'  => '1',
					'max'  => '120',
					'step' => '1',
				),
				//'mobile_options'  => true,
				//'responsive'      => true,
				'show_if'         => array(
					'map_directions' => 'on',
				),
			),
			*/
			'marker_clustering' => array(
                'label'           => esc_html__( 'Use Marker Clustering', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options' => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default' => 'off',
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
            ),
            'marker_cluster_gridsize' => array(
                'label'             => esc_html__( 'Grid Size', 'et_builder' ),
                'type'            => 'range',
				'option_category' => 'layout',
				'default_unit'    => '',
				'fixed_range' => true,
				'range_settings'  => array(
					'min'  => '10',
					'max'  => '150',
					'step' => '1',
				),
                'default' => '70',
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
				),
				'description'       => esc_html__( 'The grid size of a cluster in pixels.', 'et_builder' ),
            ),
            'marker_min_cluster_size' => array(
                'label'             => esc_html__( 'Minimum Cluster Size', 'et_builder' ),
                'type'            => 'range',
				'option_category' => 'layout',
				'default_unit'    => '',
				'fixed_range' => true,
				'range_settings'  => array(
					'min'  => '1',
					'max'  => '10',
					'step' => '1',
				),
                'default' => '2',
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
				),
				'description'       => esc_html__( 'The minimum number of markers to be in a cluster before the markers are hidden and a count is shown.', 'et_builder' ),
            ),
            'marker_clustering_styles' => array(
                'label'           => esc_html__( 'Marker Clustering Styles', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => 'default',
                'options'         => array(
                    'default' => esc_html__( 'Google (Default)', 'et_builder' ),
                    'custom_style' => esc_html__( 'Custom Styling', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
				),
            ),
            'marker_clustering_effect' => array(
                'label'           => esc_html__( 'Use Animation Effect', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options' => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default' => 'off',
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
            ),
            'marker_clustering_effect_option' => array(
                'label'           => esc_html__( 'Animation Effects', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => 'pulse',
                'options'         => array(
                    'pulse' => esc_html__( 'Pulse', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
					'marker_clustering_effect' => 'on',
				),
            ),
            'marker_clustering_pulse_color' => array(
                'label'             => esc_html__( 'Pulse Color', 'et_builder' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'default' => 'rgba(0, 140, 255, 0.4)',
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
					'marker_clustering_effect' => 'on',
					'marker_clustering_effect_option' => 'pulse',
				),
            ),
            'marker_cluster_size' => array(
                'label'             => esc_html__( 'Size', 'et_builder' ),
                'type'            => 'range',
				'option_category' => 'layout',
				'default_unit'    => 'px',
				'range_settings'  => array(
					'min'  => '20',
					'max'  => '100',
					'step' => '1',
				),
                'default' => '40px',
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
            ),
            'marker_cluster_bg' => array(
                'label'             => esc_html__( 'Background Color', 'et_builder' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'default' => '#008cff',
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
            ),
            'marker_cluster_border_color' => array(
                'label'             => esc_html__( 'Border Color', 'et_builder' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'default' => '#fff',
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
            ),
            'marker_cluster_border_width' => array(
                'label'             => esc_html__( 'Border Width', 'et_builder' ),
                'type'            => 'range',
				'option_category' => 'layout',
				'default_unit'    => 'px',
				'range_settings'  => array(
					'min'  => '0',
					'max'  => '50',
					'step' => '1',
				),
                'default' => '2px',
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
            ),
            'marker_cluster_border_style' => array(
				'label'           => esc_html__( 'Border Style', 'et_builder' ),
				'type'            => 'select',
				'option_category' => 'layout',
				'options'         => array(
					'solid'  => esc_html__( 'Solid', 'et_builder' ),
					'double' => esc_html__( 'Double', 'et_builder' ),
					'dotted' => esc_html__( 'Dotted', 'et_builder' ),
					'dashed' => esc_html__( 'Dashed', 'et_builder' ),
				),
				'default' => 'solid',
				'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'general',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
			),
            'marker_cluster_hover_text' => array(
                'label'             => esc_html__( 'Text Color', 'et_builder' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'hover',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
            ),
            'marker_cluster_hover_bg' => array(
                'label'             => esc_html__( 'Background Color', 'et_builder' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'hover',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
            ),
            'marker_cluster_hover_border_color' => array(
                'label'             => esc_html__( 'Border Color', 'et_builder' ),
                'type'              => 'color-alpha',
                'custom_color'      => true,
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'hover',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
            ),
            'marker_cluster_hover_border_width' => array(
                'label'             => esc_html__( 'Border Width', 'et_builder' ),
                'type'            => 'range',
				'option_category' => 'layout',
				'default_unit'    => 'px',
				'range_settings'  => array(
					'min'  => '0',
					'max'  => '50',
					'step' => '1',
				),
				'default' => '2px',
                'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'hover',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
            ),
            'marker_cluster_hover_border_style' => array(
				'label'           => esc_html__( 'Border Style', 'et_builder' ),
				'type'            => 'select',
				'option_category' => 'layout',
				'options'         => array(
					'solid'  => esc_html__( 'Solid', 'et_builder' ),
					'double' => esc_html__( 'Double', 'et_builder' ),
					'dotted' => esc_html__( 'Dotted', 'et_builder' ),
					'dashed' => esc_html__( 'Dashed', 'et_builder' ),
				),
				'default' => 'solid',
				'tab_slug'          => 'advanced',
                'toggle_slug'     => 'marker_cluster',
                'sub_toggle'  => 'hover',
                'show_if'         => array(
					'marker_clustering' => 'on',
					'marker_clustering_styles' => 'custom_style',
				),
			),
            'marker_filtering' => array(
                'label'           => esc_html__( 'Use Marker Filtering', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'default' => 'off',
                'options'         => array(
                    'off' => esc_html__( 'No', 'et_builder' ),
                    'on'  => esc_html__( 'Yes', 'et_builder' ),
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
			),
			/*
            'marker_filtering_before_text' => array(
				'label'           => esc_html__( 'Marker Filter Before Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'default' => 'Marker Filter By',
				'tab_slug'        => 'advanced',
                'toggle_slug'     => 'controls',
				'show_if'         => array(
					'marker_filtering' => 'on',
				),
			),
			*/
			'map_type' => array(
                'label'           => esc_html__( 'Map Type', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'default' => '1',
                'options'         => array(
                    '1'               => esc_html__( 'Roadmap (Default)', 'et_builder' ),
                    '2'               => esc_html__( 'Satellite', 'et_builder' ),
                    '3'               => esc_html__( 'Hybrid', 'et_builder' ),
                    '4'               => esc_html__( 'Terrain', 'et_builder' ),
                ),
            	'description'         => esc_html__( 'Google Maps provides four types of maps. RoadMap, Satellite, Hybird and Terrian. The default Map type is ROADMAP', 'et_builder' ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'styles',
            ),
            'map_options' => array(
                'label'           => esc_html__( 'Map Styles', 'et_builder' ),
                'type'            => 'select',
                'option_category' => 'layout',
                'options'         => array(
                    '1'               => esc_html__( 'Google Default', 'et_builder' ),
                    '2'               => esc_html__( 'Greyscale', 'et_builder' ),
                    '3'               => esc_html__( 'Shades of Grey', 'et_builder' ),
                    '4'               => esc_html__( 'Blue Water', 'et_builder' ),
                    '5'               => esc_html__( 'MarcusWithman-Map', 'et_builder' ),
                    '6'               => esc_html__( 'Table de Bellefois', 'et_builder' ),
                    '7'               => esc_html__( 'Style 04', 'et_builder' ),
                    '8'               => esc_html__( 'MapaBlanco', 'et_builder' ),
                    '9'               => esc_html__( 'decola', 'et_builder' ),
                    '10'              => esc_html__( 'Flex', 'et_builder' ),
                    '11'              => esc_html__( 'Kent Outdoors', 'et_builder' ),
                    '12'              => esc_html__( 'Transport for London', 'et_builder' ),
                    '13'              => esc_html__( 'Paper', 'et_builder' ),
                    '14'              => esc_html__( 'Light Monochrome', 'et_builder' ),
                    '15'              => esc_html__( 'Midnight Commander', 'et_builder' ),
                    '16'              => esc_html__( 'Avocado World', 'et_builder' ),
                    '17'              => esc_html__( 'Glasgow MegaSnake', 'et_builder' ),
                    '18'              => esc_html__( 'Chundo Style', 'et_builder' ),
                    '19'              => esc_html__( 'Bates Green', 'et_builder' ),
                    '20'              => esc_html__( 'mikiwat', 'et_builder' ),
                    '21'              => esc_html__( 'Bright Dessert', 'et_builder' ),
                    '22'              => esc_html__( 'coy beauty', 'et_builder' ),
                    '23'              => esc_html__( 'shades of conservation', 'et_builder' ),
                    '24'              => esc_html__( 'pixmix', 'et_builder' ),
                    '25'              => esc_html__( 'Icy Blue', 'et_builder' ),
                    '26'              => esc_html__( 'even lighter', 'et_builder' ),
                    '27'              => esc_html__( 'Bold Black & White', 'et_builder' ),
                    '28'              => esc_html__( 'Dropoff 3', 'et_builder' ),
                    '29'              => esc_html__( 'Simply Golden', 'et_builder' ),
                    '30'              => esc_html__( 'Pirate Map', 'et_builder' ),
                    '31'              => esc_html__( 'Unsaturated Browns', 'et_builder' ),
                    '32'              => esc_html__( 'Orange', 'et_builder' ),
                    '33'              => esc_html__( 'OC', 'et_builder' ),
                    '34'              => esc_html__( 'Vintage', 'et_builder' ),
                    '35'              => esc_html__( 'Calver', 'et_builder' ),
                    '36'              => esc_html__( 'Bright & Bubbly', 'et_builder' ),
                    '37'              => esc_html__( 'Red & Blue', 'et_builder' ),
                    '38'              => esc_html__( 'Argo', 'et_builder' ),
                    '39'              => esc_html__( 'Hopper', 'et_builder' ),
                    '40'              => esc_html__( 'The Propia Effect', 'et_builder' ),
                    '41'              => esc_html__( 'Cladme', 'et_builder' ),
                    '42'              => esc_html__( 'darkdetail', 'et_builder' ),
                    '43'              => esc_html__( 'Pale Dawn', 'et_builder' ),
                    '44'              => esc_html__( 'Light Green', 'et_builder' ),
                    '45'              => esc_html__( 'iovation Map', 'et_builder' ),
                    '46'              => esc_html__( 'papuportal', 'et_builder' ),
                    '47'              => esc_html__( 'Savagio Yellow', 'et_builder' ),
                    '48'              => esc_html__( 'Light Rust', 'et_builder' ),
                    '49'              => esc_html__( 'inturlam Style', 'et_builder' ),
                    '50'              => esc_html__( 'Alibra', 'et_builder' ),
                    '51'              => esc_html__( 'Dharani', 'et_builder' ),
                    '52'              => esc_html__( 'Primo', 'et_builder' ),
                    '53'              => esc_html__( 'Cotton Candy', 'et_builder' ),
                    '54'              => esc_html__( '035print', 'et_builder' ),
                    '55'              => esc_html__( 'Retro', 'et_builder' ),
                    '56'              => esc_html__( 'Avocado World', 'et_builder' ),
                    '57'              => esc_html__( 'Gowalla', 'et_builder' ),
                    '58'              => esc_html__( 'Old Timey', 'et_builder' ),
                    '59'              => esc_html__( 'The Propia Effect', 'et_builder' ),
                    '60'              => esc_html__( 'Mondrian', 'et_builder' ),
                    '61'              => esc_html__( 'Neon World', 'et_builder' ),
                    '62'              => esc_html__( 'Old Map', 'et_builder' ),
                    '63'              => esc_html__( 'Flat Pale', 'et_builder' ),
                    '64'              => esc_html__( 'Candy Colours', 'et_builder' ),
                    '65'              => esc_html__( 'Old-School maps posters', 'et_builder' ),
                    '66'              => esc_html__( 'Camilo florez estilo de mapa modificado', 'et_builder' ),
                    '67'              => esc_html__( 'Lemon Tree', 'et_builder' ),
                    '68'              => esc_html__( 'Grayscale Yellow', 'et_builder' ),
                    '69'              => esc_html__( 'Presto Map', 'et_builder' ),
                    '70'              => esc_html__( 'Best Ski Pros', 'et_builder' ),
                    '71'              => esc_html__( 'Purple', 'et_builder' ),
                    '72'              => esc_html__( 'Green', 'et_builder' ),
                    '73'              => esc_html__( 'Squarespace Style', 'et_builder' ),
                    '74'              => esc_html__( 'Blue Essence', 'et_builder' ),
                ),
                'description'       => esc_html__( 'Choose a map design. This module uses Snazzy Maps data.', 'et_builder' ),
                'class' => array( 'map_options' ),
                'default' => '1',
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'styles',
                'show_if_not'         => array(
					'map_custom_style' => 'on',
				),
            ),
            'map_custom_style' => array(
                'label'           => esc_html__( 'Use Custom Google Map Code', 'et_builder' ),
                'type'            => 'yes_no_button',
                'option_category' => 'configuration',
                'options' => array(
                    'on'  => esc_html__( 'On', 'et_builder' ),
                    'off' => esc_html__( 'Off', 'et_builder' ),
                ),
                'default' => 'off',
                'default_on_front' => 'off',
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'styles',
                'description'       => sprintf( __( 'You can paste your Google Map JSON code from <a href="%1$s" target="_blank">Google</a>. or <a href="%2$s" target="_blank">SnazzyMap</a>. This is overwrite any of the map styles above.', 'et_builder' ), esc_url( 'https://mapstyle.withgoogle.com' ), esc_url( 'https://snazzymaps.com' ) ),
            ),
            'map_custom_code' => array(
				'label'           => esc_html__( 'Custom Google Map Style Code', 'et_builder' ),
				'type'            => 'codemirror',
				'mode'            => 'html',
				'option_category' => 'basic_option',
				'description'       => sprintf( __( 'Paste your Google Style Code. It is best to minify those code using Minifier <a href="%1$s" target="_blank">here</a>.', 'et_builder' ), esc_url( 'https://kangax.github.io/html-minifier/' ) ),
				'tab_slug'        => 'advanced',
                'toggle_slug'     => 'styles',
                'show_if'         => array(
					'map_custom_style' => 'on',
				),
			),
			'map_height' => array(
				'label'           => esc_html__( 'Map Height', 'et_builder' ),
				'type'            => 'range',
				'option_category' => 'layout',
				'default_unit'    => 'px',
				'range_settings'  => array(
					'min'  => '200',
					'max'  => '1000',
					'step' => '1',
				),
				'mobile_options'  => true,
				'responsive'      => true,
				'tab_slug'        => 'advanced',
				'toggle_slug'       => 'width',
				'description'     => esc_html__( 'This defines the height of the Map.', 'et_builder' ),
			),
		);
	}

	public function render( $attrs, $content = null, $render_slug ) {
		$address_lat             = $this->props['address_lat'];
		$address_lng             = $this->props['address_lng'];
		$zoom_level              = $this->props['zoom_level'];
		//$mouse_wheel             = $this->props['mouse_wheel'];
		$mobile_dragging         = $this->props['mobile_dragging'];
		$use_grayscale_filter    = $this->props['use_grayscale_filter'];
		$grayscale_filter_amount = $this->props['grayscale_filter_amount'];
		$gesture_handling = $this->props['gesture_handling'];
		$controls_ui = $this->props['controls_ui'];
		$map_type_control_style = $this->props['map_type_control_style'];
		$map_type_control_position = $this->props['map_type_control_position'];
		$zoom_control_position = $this->props['zoom_control_position'];
		$streetview_control_position = $this->props['streetview_control_position'];
		$marker_center = $this->props['marker_center'];
		$marker_center_zoom = $this->props['marker_center_zoom'];
		$infowindow_styles = $this->props['infowindow_styles'];
		$infowindow_radius = $this->props['infowindow_radius'];
		$infowindow_close_icon = $this->props['infowindow_close_icon'];
		$infowindow_close_color = $this->props['infowindow_close_color'];
		$infowindow_bg_color = $this->props['infowindow_bg_color'];
		$infowindow_animation_in = $this->props['infowindow_animation_in'];
		$info_window_width = $this->props['info_window_width'];
		$info_window = $this->props['info_window'];
		$info_window_method = $this->props['info_window_method'];
		$bounce_on_click_on_off = $this->props['bounce_on_click_on_off'];
		$marker_animation = $this->props['marker_animation'];
		$marker_auto_center = $this->props['marker_auto_center'];
		$marker_label_all = $this->props['marker_label_all'];
		$marker_label_bg = $this->props['marker_label_bg'];
		$marker_label_radius = $this->props['marker_label_radius'];
		$marker_label_padding = $this->props['marker_label_padding'];
		$marker_label_padding_tablet      = $this->props['marker_label_padding_tablet'];
		$marker_label_padding_phone       = $this->props['marker_label_padding_phone'];
		$marker_label_padding_last_edited = $this->props['marker_label_padding_last_edited'];
		$show_marker_listing = $this->props['show_marker_listing'];
		$show_marker_listing_icon = $this->props['show_marker_listing_icon'];
		$map_marker_listing_zoom = $this->props['map_marker_listing_zoom'];
		//$show_marker_listing_title = $this->props['show_marker_listing_title'];
		$marker_listing_title = $this->props['marker_listing_title'];
		$show_marker_listing_address = $this->props['show_marker_listing_address'];
		$marker_listing_address = $this->props['marker_listing_address'];
		$show_marker_listing_description = $this->props['show_marker_listing_description'];
		$marker_listing_description = $this->props['marker_listing_description'];
		$show_marker_listing_direction = $this->props['show_marker_listing_direction'];
		$marker_listing_direction = $this->props['marker_listing_direction'];
		$show_marker_listing_category = $this->props['show_marker_listing_category'];
		$marker_listing_category = $this->props['marker_listing_category'];
		$marker_listing_bg_thead = $this->props['marker_listing_bg_thead'];
		$marker_listing_odd_bg = $this->props['marker_listing_odd_bg'];
		$marker_listing_bg = $this->props['marker_listing_bg'];
		$marker_listing_bg_hover = $this->props['marker_listing_bg_hover'];
		$marker_listing_border_color = $this->props['marker_listing_border_color'];
		$listing_pagi_current_background = $this->props['listing_pagi_current_background'];
		$listing_pagi_current_background_hover = $this->get_hover_value( 'listing_pagi_current_background' );
		$listing_pagi_page_num_background = $this->props['listing_pagi_page_num_background'];
		$listing_pagi_page_num_background_hover = $this->get_hover_value( 'listing_pagi_page_num_background' );
		$listing_pagi_border  = $this->props['listing_pagi_border'];
		//$custom_icon_1 = $this->props['button_one_icon'];
		//$button_custom_1 = $this->props['custom_button_one'];
		$map_directions = $this->props['map_directions'];
		$map_direction_location_icon = $this->props['map_direction_location_icon'];
		$map_direction_current_location_text = $this->props['map_direction_current_location_text'];
		//$map_direction_location_icon_size = $this->props['map_direction_location_icon_size'];
		$map_directions_placement = $this->props['map_directions_placement'];
		$map_directions_text = $this->props['map_directions_text'];
		$marker_clustering = $this->props['marker_clustering'];
		$marker_cluster_gridsize = $this->props['marker_cluster_gridsize'];
		$marker_min_cluster_size = $this->props['marker_min_cluster_size'];
		$marker_clustering_styles = $this->props['marker_clustering_styles'];
		$marker_clustering_effect = $this->props['marker_clustering_effect'];
		$marker_clustering_effect_option = $this->props['marker_clustering_effect_option'];
		$marker_clustering_pulse_color = $this->props['marker_clustering_pulse_color'];
		$marker_cluster_size = $this->props['marker_cluster_size'];
		$marker_cluster_bg = $this->props['marker_cluster_bg'];
		$marker_cluster_border_color = $this->props['marker_cluster_border_color'];
		$marker_cluster_border_width = $this->props['marker_cluster_border_width'];
		$marker_cluster_border_style = $this->props['marker_cluster_border_style'];
		$marker_cluster_hover_text = $this->props['marker_cluster_hover_text'];
		$marker_cluster_hover_bg = $this->props['marker_cluster_hover_bg'];
		$marker_cluster_hover_border_color = $this->props['marker_cluster_hover_border_color'];
		$marker_cluster_hover_border_width = $this->props['marker_cluster_hover_border_width'];
		$marker_cluster_hover_border_style = $this->props['marker_cluster_hover_border_style'];
		$marker_filtering = $this->props['marker_filtering'];
		//$marker_filtering_before_text = $this->props['marker_filtering_before_text'];
		$map_height = $this->props['map_height'];
		$map_height_tablet      = $this->props['map_height_tablet'];
		$map_height_phone       = $this->props['map_height_phone'];
		$map_height_last_edited = $this->props['map_height_last_edited'];
		$map_options = $this->props['map_options'];
		$map_custom_style = $this->props['map_custom_style'];
		$map_type = $this->props['map_type'];
		$map_custom_code = et_builder_replace_code_content_entities( $this->props['map_custom_code'] );
		$dwd_replace_code_content = strip_tags($map_custom_code);
  		$new_dwd_replace_code_content = preg_replace('/\s/', '', $dwd_replace_code_content);

		if ( et_pb_enqueue_google_maps_script() ) {
				wp_enqueue_script( 'google-maps-api' );
		}

		$video_background          = $this->video_background();
		$parallax_image_background = $this->get_parallax_image_background();

		$all_pins_content = $this->content;

		global $et_pb_tab_titles;
		global $et_pb_pin_classes;
		global $et_pb_pin_address_get;
		global $et_pb_pin_lat;
		global $et_pb_pin_lng;
		global $et_pb_content;
		global $et_pb_pin_on_off;
		global $et_pb_pin_src;
		global $et_pb_pin_widthsize;
		global $et_pb_pin_heightsize;
		global $et_pb_pin_fill_color;
		global $et_pb_pin_stroke_color;
		global $et_pb_pin_circle_color;
		global $et_pb_pin_fill_color_hover;
		global $et_pb_pin_stroke_color_hover;
		global $et_pb_pin_circle_color_hover;
		global $et_pb_pin_label_text;
		global $et_pb_pin_website_text;
		global $et_pb_pin_website_url;
		global $et_pb_pin_email_text;
		global $et_pb_pin_email_url;
		global $et_pb_pin_url;
		global $et_pb_pin_url_new_window;
		global $et_pb_pin_url_trigger_method;
		global $et_pb_marker_category;
		global $et_pb_marker_category_name;
		global $et_pb_marker_filter_category_built_in;
		global $et_pb_pin_cover_image_src;
		global $et_pb_pin_change_src_on_off;
		global $et_pb_pin_change_src;
		global $et_pb_pin_change_widthsize;
		global $et_pb_pin_change_heightsize;

		$pins_address_output = '';
		$pins_table = '';
		$marker_filtering_output_loop = '';

		$dwd_marker_category_tax = get_terms( array(
            'taxonomy' => 'divi_map_category',
            'hide_empty' => false,
        ));

		if ($et_pb_marker_category_name !== null) {
			$et_pb_marker_category_sort = explode(',', implode(',', $et_pb_marker_category_name));
			$et_pb_marker_category_sorted = array_unique(array_filter($et_pb_marker_category_sort));

	        foreach($et_pb_marker_category_sorted as $categories) {
	        	$marker_filtering_output_loop .= sprintf( '<option value="%1$s">%1$s</option>',
					esc_html( $categories )
				);
	    	}
		}

		$i = 0;

		$marker_listing_thead_output = sprintf('
			<thead>
				<tr class="dwd_map_thead_main" data-marker-listing-title="%1$s" data-marker-listing-category="%5$s" data-marker-listing-address="%2$s" data-marker-listing-description="%3$s" data-marker-listing-direction="%4$s">
					<th class="dwd-marker-listing-thead">
						<span>%1$s</span>
					</th>
					%9$s
					%6$s
					<th class="dwd-marker-listing-thead%7$s">
						<span>%3$s</span>
					</th>
					%8$s
				</tr>
			</thead>',
		    esc_attr( $marker_listing_title ),
		    esc_attr( $marker_listing_address ),
		    esc_attr( $marker_listing_description ),
		    esc_attr( $marker_listing_direction ),
			esc_attr( $marker_listing_category ),
			( 'off' !== $show_marker_listing_address ? sprintf( '<th class="dwd-marker-listing-thead%2$s"><span>%1$s</span></th>',
				esc_html( $marker_listing_address ),
				( 'on' !== $show_marker_listing_address ? ' et_pb_map_pin' : '' )
			) : '' ),
			( 'on' !== $show_marker_listing_description ? ' et_pb_map_pin' : '' ),
			( 'off' !== $map_directions ? sprintf( '<th class="dwd-marker-listing-thead%2$s"><span>%1$s</span></th>',
				esc_html( $marker_listing_direction ),
				( 'on' !== $show_marker_listing_direction ? ' et_pb_map_pin' : '' )
			) : '' ),
			( 'off' !== $marker_filtering ? sprintf( '<th class="dwd-marker-listing-thead dwd-marker-category-listing%2$s">
				<span>%1$s</span>
			</th>',
				esc_html( $marker_listing_category ),
				( 'on' !== $show_marker_listing_category ? ' et_pb_map_pin' : '' )
			) : '' )
		);

		$pins_table .= $marker_listing_thead_output;
		$pins_table .= '<tbody>';
		
		if ( ! empty( $et_pb_tab_titles ) ) {
			foreach ( $et_pb_tab_titles as $pin_title ){
				++$i;
				$pins_address_output .= sprintf( '<option value="%2$s" data-lat="%3$s" data-lng="%4$s">To: %1$s</option>',
					esc_html( $pin_title ),
					esc_html( ltrim( $et_pb_pin_address_get[ $i-1 ] ) ),
					esc_attr( ltrim( $et_pb_pin_lat[ $i-1 ] ) ),
					esc_attr( ltrim( $et_pb_pin_lng[ $i-1 ] ) )
				);
			
				$pins_table .= sprintf( '<tr class="et_pb_map_pin_extended %4$s" data-marker-filter-category="%13$s"><td class="dwd_map_pin" data-lat="%2$s" data-lng="%3$s" data-title="%1$s" data-marker-listing-title="%16$s" %5$s%10$s%11$s%12$s%14$s%18$s data-marker-filter-category="%13$s"><div class="dwd-markerlisting-title-wrapper">%15$s%17$s</div></td>%6$s%7$s%8$s%9$s</tr>',
					esc_html( $pin_title ),
					esc_attr( ltrim( $et_pb_pin_lat[ $i-1 ] ) ),
					esc_attr( ltrim( $et_pb_pin_lng[ $i-1 ] ) ),
					esc_attr( ltrim( $et_pb_pin_classes[ $i-1 ] ) ),
					( 'off' !== ltrim( $et_pb_pin_on_off[ $i-1 ] ) ? sprintf( ' data-pin-custom-image="on" data-pin-src="%1$s" data-pin-width="%2$s" data-pin-height="%3$s"',
						esc_url( ltrim( $et_pb_pin_src[ $i-1 ] ) ),
						floatval( esc_attr( ltrim( $et_pb_pin_widthsize[ $i-1 ] ) ) ),
						floatval( esc_attr( ltrim( $et_pb_pin_heightsize[ $i-1 ] ) ) )
					) : sprintf( ' data-pin-fill-color="%1$s" data-pin-stroke-color="%2$s" data-pin-circle-color="%3$s"%4$s%5$s%6$s',
						esc_attr( ltrim( $et_pb_pin_fill_color[ $i-1 ] ) ),
						esc_attr( ltrim( $et_pb_pin_stroke_color[ $i-1 ] ) ),
						esc_attr( ltrim( $et_pb_pin_circle_color[ $i-1 ] ) ),
						( '' !== esc_attr( ltrim( $et_pb_pin_fill_color_hover[ $i-1 ] ) ) ? sprintf( ' data-pin-fill-hover-color="%1$s"',
							esc_attr( ltrim( $et_pb_pin_fill_color_hover[ $i-1 ] ) )
						) : '' ),
						( '' !== esc_attr( ltrim( $et_pb_pin_stroke_color_hover[ $i-1 ] ) ) ? sprintf( ' data-pin-stroke-hover-color="%1$s"',
							esc_attr( ltrim( $et_pb_pin_stroke_color_hover[ $i-1 ] ) )
						) : '' ),
						( '' !== esc_attr( ltrim( $et_pb_pin_circle_color_hover[ $i-1 ] ) ) ? sprintf( ' data-pin-circle-hover-color="%1$s"',
							esc_attr( ltrim( $et_pb_pin_circle_color_hover[ $i-1 ] ) )
						) : '' )
					) ),
					( 'off' !== $marker_filtering ? sprintf( '<td%2$s data-marker-listing-category="%3$s"><span>%1$s</span></td>',
						( '' !== esc_attr( ltrim( $et_pb_marker_category[ $i-1 ] ) ) ? esc_attr( ltrim( $et_pb_marker_category[ $i-1 ] ) ) : '&nbsp' ),
						( 'on' !== $show_marker_listing_category ? ' class="et_pb_map_pin"' : '' ),
						esc_attr( $marker_listing_category )
					) : '' ),
					( 'off' !== $show_marker_listing_address ? sprintf( '<td%2$s data-marker-listing-address="%3$s"><span>%1$s</span></td>',
						esc_attr( ltrim( $et_pb_pin_address_get[ $i-1 ] ) ),
						( 'on' !== $show_marker_listing_address ? ' class="et_pb_map_pin"' : '' ),
						esc_attr( $marker_listing_address )
					) : '' ),
					sprintf( '<td class="infowindow%2$s" data-marker-listing-description="%3$s"><span>%1$s</span></td>',
						ltrim( $et_pb_content[ $i-1 ] ),
						( 'on' !== $show_marker_listing_description ? ' et_pb_map_pin' : '' ),
						esc_attr( $marker_listing_description )
					),
					( 'off' !== $map_directions ? sprintf( '<td class="dwd-marker-listing-direction%2$s" data-marker-listing-direction="%3$s"><span>%1$s</span></td>',
						esc_html( $marker_listing_direction ),
						( 'on' !== $show_marker_listing_direction ? ' et_pb_map_pin' : '' ),
						esc_attr( $marker_listing_direction )
					) : '' ),
					( '' !== esc_html( ltrim( $et_pb_pin_label_text[ $i-1 ] ) ) ? sprintf( ' data-marker-label-text="%1$s"', esc_html( ltrim( $et_pb_pin_label_text[ $i-1 ] ) ) ) : '' ),
					( '' !== esc_attr( ltrim( $et_pb_pin_website_text[ $i-1 ] ) ) || '' !== esc_attr( ltrim( $et_pb_pin_email_text[ $i-1 ] ) ) ? sprintf( ' data-pin-website-text="%1$s" data-pin-website-url="%2$s" data-pin-email-text="%3$s" data-pin-email="%4$s"',
						esc_attr( ltrim( $et_pb_pin_website_text[ $i-1 ] ) ),
						esc_url( ltrim( $et_pb_pin_website_url[ $i-1 ] ) ),
						esc_attr( ltrim( $et_pb_pin_email_text[ $i-1 ] ) ),
						esc_url( 'mailto:' . ltrim( $et_pb_pin_email_url[ $i-1 ] ) )
					) : '' ),
					( '' !== esc_url( ltrim( $et_pb_pin_url[ $i-1 ] ) ) ? sprintf( ' data-marker-url-link="%1$s" data-marker-url-window="%2$s" data-marker-url-trigger-method="%3$s"',
						esc_url( ltrim( $et_pb_pin_url[ $i-1 ] ) ),
						esc_attr( ltrim( $et_pb_pin_url_new_window[ $i-1 ] ) ),
						esc_attr( ltrim( $et_pb_pin_url_trigger_method[ $i-1 ] ) )
					) : '' ),
					( '' !== esc_attr( ltrim( $et_pb_marker_filter_category_built_in[ $i-1 ] ) ) ? sprintf( '%1$s',
						esc_attr( ltrim( $et_pb_marker_category[ $i-1 ] ) )
					) : '' ),
					( '' !== esc_url( ltrim( $et_pb_pin_cover_image_src[ $i-1 ] ) ) ? sprintf( ' data-pin-cover-image="%1$s"',
						esc_url( ltrim( $et_pb_pin_cover_image_src[ $i-1 ] ) )
					) : '' ),
					( 'off' !== $show_marker_listing && 'off' !== $show_marker_listing_icon ? sprintf( '%1$s',
						( 'off' !== ltrim( $et_pb_pin_on_off[ $i-1 ] ) ? sprintf( '<div class="dwd-map-marker-listing-icon"><img src="%1$s" width="%2$s" height="%3$s"></div>',
							esc_url( ltrim( $et_pb_pin_src[ $i-1 ] ) ),
							floatval( esc_attr( ltrim( $et_pb_pin_widthsize[ $i-1 ] ) ) ),
							floatval( esc_attr( ltrim( $et_pb_pin_heightsize[ $i-1 ] ) ) )
						) : sprintf( '<div class="dwd-map-marker-listing-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="30" viewBox="-16 -4 32 32"><path fill="%1$s" stroke="%2$s" stroke-width="1" d="M0,40 Q0,28 10,15 A15,15 0,1,0 -10,15 Q0,28 0,40"/><circle cx="0" cy="5" r="5" fill="%3$s" stroke="none"/></svg></div>',
							esc_attr( ltrim( $et_pb_pin_fill_color[ $i-1 ] ) ),
							esc_attr( ltrim( $et_pb_pin_stroke_color[ $i-1 ] ) ),
							esc_attr( ltrim( $et_pb_pin_circle_color[ $i-1 ] ) )
						) )
					) : '' ),
					//data attr thead
					esc_attr( $marker_listing_title ),
					//title
					( '' !== esc_html( $pin_title ) ? sprintf( '<span class="dwd-infowindow-title">%1$s</span>', esc_html( $pin_title ) ) : '' ),
					//change pin on click/hover
					( 'off' !== ltrim( $et_pb_pin_change_src_on_off[ $i-1 ] ) ? sprintf( ' data-pin-change-custom-image="on" data-pin-change-src="%1$s" data-pin-change-width="%2$s" data-pin-change-height="%3$s"',
						esc_url( ltrim( $et_pb_pin_change_src[ $i-1 ] ) ),
						floatval( esc_attr( ltrim( $et_pb_pin_change_widthsize[ $i-1 ] ) ) ),
						floatval( esc_attr( ltrim( $et_pb_pin_change_heightsize[ $i-1 ] ) ) )
					) : '' )
				);
				/*
				$marker_filtering_output_loop .= sprintf( '<option value="%1$s">%1$s</option>',
					esc_html( ltrim( $et_pb_marker_category_sorteds[ $i-1 ] ) )
				);*/
			}

		}
		$pins_table .= '</tbody>';

		$et_pb_tab_titles = $et_pb_pin_classes = $et_pb_content = $et_pb_pin_website_text = $et_pb_pin_website_url = $et_pb_pin_email_text = $et_pb_pin_email_url = $et_pb_pin_address_get = $et_pb_pin_lat = $et_pb_pin_lng = $et_pb_pin_on_off = $et_pb_pin_src = $et_pb_pin_widthsize = $et_pb_pin_heightsize = $et_pb_pin_fill_color = $et_pb_pin_fill_color_hover = $et_pb_pin_stroke_color = $et_pb_pin_stroke_color_hover = $et_pb_pin_circle_color = $et_pb_pin_circle_color_hover = $et_pb_pin_label_text = $et_pb_pin_url = $et_pb_pin_url_new_window = $et_pb_pin_url_trigger_method = $et_pb_marker_category = $et_pb_marker_category_name = $et_pb_marker_filter_category_built_in = $et_pb_pin_cover_image_src = $et_pb_pin_change_src_on_off = $et_pb_pin_change_src = $et_pb_pin_change_widthsize = $et_pb_pin_change_heightsize = array();

		$grayscale_filter_data = '';
		if ( 'on' === $use_grayscale_filter && '' !== $grayscale_filter_amount ) {
			$grayscale_filter_data = sprintf( ' data-grayscale="%1$s"', esc_attr( $grayscale_filter_amount ) );
		}

		// Map Tiles: Add CSS Filters and Mix Blend Mode rules (if set)
		if ( array_key_exists( 'child_filters', $this->advanced_fields ) && array_key_exists( 'css', $this->advanced_fields['child_filters'] ) ) {
			$this->add_classname( $this->generate_css_filters(
				$render_slug,
				'child_',
				self::$data_utils->array_get( $this->advanced_fields['child_filters']['css'], 'main', '%%order_class%%' )
			) );
		}

		if ( '' !== $marker_label_bg ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-map-label span',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_attr( $marker_label_bg )
				),
			) );
		}

		if ( '' !== $marker_label_radius ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-map-label span',
				'declaration' => sprintf(
					'border-radius: %1$s;',
					esc_attr( $marker_label_radius )
				),
			) );
		}

		if ( '40px' !== $marker_cluster_size ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster',
				'declaration' => sprintf(
					'width: %1$s;
					height: %1$s;',
					esc_attr( $marker_cluster_size )
				),
			) );
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster.dwd-markercluster-pulse:after',
				'declaration' => sprintf(
					'width: %1$s;
					height: %1$s;',
					esc_attr( $marker_cluster_size )
				),
			) );
		}

		if ( 'rgba(0, 140, 255, 0.4)' !== $marker_clustering_pulse_color ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster.dwd-markercluster-pulse:after',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_attr( $marker_clustering_pulse_color )
				),
			) );
		}

		if ( '#008cff' !== $marker_cluster_bg ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_attr( $marker_cluster_bg )
				),
			) );
		}

		if ( '#ffffff' !== $marker_cluster_border_color ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster',
				'declaration' => sprintf(
					'border-color: %1$s;',
					esc_attr( $marker_cluster_border_color )
				),
			) );
		}

		if ( '2px' !== $marker_cluster_border_width ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster',
				'declaration' => sprintf(
					'border-width: %1$s;',
					esc_attr( $marker_cluster_border_width )
				),
			) );
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster.dwd-markercluster-pulse:after',
				'declaration' => sprintf(
					'left: -%1$s;',
					esc_attr( $marker_cluster_border_width )
				),
			) );
		}

		if ( 'solid' !== $marker_cluster_border_style ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster',
				'declaration' => sprintf(
					'border-style: %1$s;',
					esc_attr( $marker_cluster_border_style )
				),
			) );
		}

		if ( '' !== $marker_cluster_hover_text ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster:hover',
				'declaration' => sprintf(
					'color: %1$s;',
					esc_attr( $marker_cluster_hover_text )
				),
			) );
		}

		if ( '' !== $marker_cluster_hover_bg ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster:hover',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_attr( $marker_cluster_hover_bg )
				),
			) );
		}

		if ( '' !== $marker_cluster_hover_border_color ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster:hover',
				'declaration' => sprintf(
					'border-color: %1$s;',
					esc_attr( $marker_cluster_hover_border_color )
				),
			) );
		}

		if ( '' !== $marker_cluster_hover_border_width ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster:hover',
				'declaration' => sprintf(
					'border-width: %1$s;',
					esc_attr( $marker_cluster_hover_border_width )
				),
			) );
		}

		if ( '' !== $marker_cluster_hover_border_style ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd-markercluster:hover',
				'declaration' => sprintf(
					'border-style: %1$s;',
					esc_attr( $marker_cluster_hover_border_style )
				),
			) );
		}

		if ( '' !== $map_height || '' !== $map_height_tablet || '' !== $map_height_phone ) {
			$map_height_responsive_active = et_pb_get_responsive_status( $map_height_last_edited );

			$map_height_values = array(
				'desktop' => $map_height,
				'tablet'  => $map_height_responsive_active ? $map_height_tablet : '',
				'phone'   => $map_height_responsive_active ? $map_height_phone : '',
			);

			et_pb_generate_responsive_css( $map_height_values, '%%order_class%% .dwd_map_extended', 'height', $render_slug );
		}

		if ( '' !== $marker_label_padding_tablet || '' !== $marker_label_padding_phone || '' !== $marker_label_padding ) {
        	$dwd_marker_label_padding = array( '', '', '', '' );
			foreach(explode("|", $marker_label_padding) as $key => $val) {
				if ($key === 0 && '' !== $val) {
					$dwd_marker_label_padding['padding-top'] = $val;
				}
				if ($key === 1 && '' !== $val) {
					$dwd_marker_label_padding['padding-right'] = $val;
				}
				if ($key === 2 && '' !== $val) {
					$dwd_marker_label_padding['padding-bottom'] = $val;
				}
				if ($key === 3 && '' !== $val) {
					$dwd_marker_label_padding['padding-left'] = $val;
				}
			}

			$marker_label_padding = $dwd_marker_label_padding;

			$dwd_marker_label_padding_tablet = array( '', '', '', '' );
			foreach(explode("|", $marker_label_padding_tablet) as $key => $val) {
				if ($key === 0 && '' !== $val) {
					$dwd_marker_label_padding_tablet['padding-top'] = $val;
				}
				if ($key === 1 && '' !== $val) {
					$dwd_marker_label_padding_tablet['padding-right'] = $val;
				}
				if ($key === 2 && '' !== $val) {
					$dwd_marker_label_padding_tablet['padding-bottom'] = $val;
				}
				if ($key === 3 && '' !== $val) {
					$dwd_marker_label_padding_tablet['padding-left'] = $val;
				}
			}

			$marker_label_padding_tablet = $dwd_marker_label_padding_tablet;

			$dwd_marker_label_padding_phone = array( '', '', '', '' );
			foreach(explode("|", $marker_label_padding_phone) as $key => $val) {
				if ($key === 0 && '' !== $val) {
					$dwd_marker_label_padding_phone['padding-top'] = $val;
				}
				if ($key === 1 && '' !== $val) {
					$dwd_marker_label_padding_phone['padding-right'] = $val;
				}
				if ($key === 2 && '' !== $val) {
					$dwd_marker_label_padding_phone['padding-bottom'] = $val;
				}
				if ($key === 3 && '' !== $val) {
					$dwd_marker_label_padding_phone['padding-left'] = $val;
				}
			}

			$marker_label_padding_phone = $dwd_marker_label_padding_phone;

			$marker_label_padding_responsive_active = et_pb_get_responsive_status( $marker_label_padding_last_edited );
			$marker_label_padding_values = array(
				'desktop' => $marker_label_padding,
				'tablet'  => $marker_label_padding_responsive_active ? $marker_label_padding_tablet : '',
				'phone'   => $marker_label_padding_responsive_active ? $marker_label_padding_phone : '',
			);

			et_pb_generate_responsive_css( $marker_label_padding_values, '%%order_class%% .dwd-map-label span', 'padding', $render_slug );
		}

		//InfoWindow
		if ( 'custom_style' == $infowindow_styles ) {
			if ( '' !== $infowindow_radius ) {
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .dwd-infowindow',
					'declaration' => sprintf(
						'border-radius: %1$s;',
						esc_attr( $infowindow_radius )
					),
				) );
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .dwd-infowindow .dwd-map-cover-image',
					'declaration' => sprintf(
						'border-top-left-radius: %1$s; border-top-right-radius: %1$s;',
						esc_attr( $infowindow_radius )
					),
				) );
			}
			if ( '' !== $infowindow_close_color ) {
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .dwd-infowindow .dwd-map-close',
					'declaration' => sprintf(
						'color: %1$s;',
						esc_attr( $infowindow_close_color )
					),
				) );
			}
			if ( '' !== $infowindow_bg_color ) {
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .dwd-infowindow',
					'declaration' => sprintf(
						'background-color: %1$s;',
						esc_attr( $infowindow_bg_color )
					),
				) );
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .dwd-infowindow:after',
					'declaration' => sprintf(
						'border-top: 10px solid %1$s;',
						esc_attr( $infowindow_bg_color )
					),
				) );
			}
		}

		//Marker Listing
		if ( '#f9fafb' !== $marker_listing_bg_thead ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dwd_map_extended_child .dwd-marker-listing-thead',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_attr( $marker_listing_bg_thead )
				),
			) );
		}

		if ( '' !== $marker_listing_bg ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% table.dwd_map_extended_child .et_pb_map_pin_extended.even',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_attr( $marker_listing_bg )
				),
			) );
		}
		if ( '#ffffff' !== $marker_listing_odd_bg ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% table.dwd_map_extended_child .et_pb_map_pin_extended.odd',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_attr( $marker_listing_odd_bg )
				),
			) );
		}

		if ( '#f4f4f4' !== $marker_listing_bg_hover ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% table.dwd_map_extended_child .et_pb_map_pin_extended:hover',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_attr( $marker_listing_bg_hover )
				),
			) );
		}

		if ( 'rgba(34,36,38,.15)' !== $listing_pagi_border ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dataTables_wrapper .dataTables_paginate',
				'declaration' => sprintf(
					'border: 1px solid %1$s;',
					esc_html( $listing_pagi_border )
				),
			) );
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dataTables_wrapper .dataTables_paginate .paginate_button:before',
				'declaration' => sprintf(
					'background: %1$s;',
					esc_html( $listing_pagi_border )
				),
			) );
		} 

		if ( 'rgba(0,0,50,.02)' !== $listing_pagi_current_background ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dataTables_wrapper .dataTables_paginate .paginate_button.current',
				'declaration' => sprintf(
					'background: %1$s;',
					esc_html( $listing_pagi_current_background )
				),
			) );
		}
		if ( '' !== $listing_pagi_current_background_hover ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover',
				'declaration' => sprintf(
					'background: %1$s;',
					esc_html( $listing_pagi_current_background_hover )
				),
			) );
		}
		if ( '' !== $listing_pagi_page_num_background ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dataTables_wrapper .dataTables_paginate .paginate_button',
				'declaration' => sprintf(
					'background: %1$s;',
					esc_html( $listing_pagi_page_num_background )
				),
			) );
		}
		if ( '' !== $listing_pagi_page_num_background_hover ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .dataTables_wrapper .dataTables_paginate .paginate_button:hover',
				'declaration' => sprintf(
					'background: %1$s;',
					esc_html( $listing_pagi_page_num_background_hover )
				),
			) );
		}

		if ( 'rgba(34,36,38,.15)' !== $marker_listing_border_color ) {

			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% table.dwd_map_extended_child',
				'declaration' => sprintf(
					'border: 1px solid %1$s;',
					esc_attr( $marker_listing_border_color )
				),
			) );
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% table.dwd_map_extended_child tr td',
				'declaration' => sprintf(
					'border-top: 1px solid %1$s;',
					esc_attr( $marker_listing_border_color )
				),
			) );
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '@media all and (max-width: 767px) { %%order_class%% .et_pb_map_pin_extended',
				'declaration' => sprintf(
					'border-bottom: 1px solid %1$s; }',
					esc_attr( $marker_listing_border_color )
				),
			) );
		}


		// Module classnames
		$this->add_classname( array(
			'dwd_map',
			( 'off' !== $show_marker_listing ? 'dwd-marker-listing' : '' )
		) );

		$this->remove_classname( $render_slug );
			
		if ($marker_clustering == 'on') {
			wp_enqueue_script( 'dwd-marker-clustering' );
		}
		wp_enqueue_script( 'dwd-infobox' );
		wp_enqueue_script( 'dwd-marker-with-label' );
		wp_enqueue_script( 'dwd-map-extended' );
		if ($show_marker_listing == 'on' || $marker_filtering == 'on') {
			wp_enqueue_script( 'dwd-nice-select' );
		}
		if ($show_marker_listing == 'on') {
			wp_enqueue_script( 'dwd-datatables' );
		}


		$map_direction_output = sprintf( '
			<div class="dwd-map-direction-dialog dwd-map-direction-%1$s" data-map-direction-current-location-text="%4$s">
          		<div class="dwd-map-direction-wrapper">
            		<div class="dwd-map-location-wrapper">
            			<input type="text" class="dwd-map-routeFrom" name="dwd-map-routeFrom" />
    				  	<button class="dwd-map-get-address">
    				  		<span class="et-pb-icon">%2$s</span>
    				  	</button>
            		</div>
  					<select class="dwd-map-routeTo" name="routeTo">
  						%3$s
  					</select>
	  				<select class="dwd-map-routeMode" name="dwd-map-routeMode">
	  				    <option value="DRIVING">%5$s: Driving</option>
	  				    <option value="WALKING">%5$s: Walking</option>
	  				    <option value="BICYCLING">%5$s: Bicycling</option>
	  				    <option value="TRANSIT">%5$s: Transit</option>
	  				</select>
				  	<button class="dwd-map-routeGo">%6$s</button>
			  	</div>
      		</div>
      		<div class="dwd-directions"></div>',
			esc_attr( $map_directions_placement ),
			esc_attr( et_pb_process_font_icon( $map_direction_location_icon ) ),
			$pins_address_output,
			esc_attr( $map_direction_current_location_text ),
			esc_html__( 'Mode', 'dwd-map-extended' ),
			esc_html__( 'Route', 'dwd-map-extended' )
		);


		$marker_listing_filtering_output = sprintf('
			<div class="dwd-map-marker-length">
				<div class="dwd-map-marker-filter">
					<select name="dwd_length_change" id="dwd_length_change" class="dwd-map-select small">
						<option value="-1">%1$s</option>
						<option value="2">2 %2$s</option>
						<option value="5">5 %2$s</option>
						<option value="10">10 %2$s</option>
						<option value="25">25 %2$s</option>
						<option value="50">50 %2$s</option>
						<option value="100">100 %2$s</option>
					</select>
				</div>
			</div>
			',
			esc_html__( 'All Entries', 'dwd-map-extended' ),
			esc_html__( 'Entries', 'dwd-map-extended' )
		);

		$marker_filtering_on_off = '';
		if ( 'off' !== $marker_filtering ) {
			$marker_filtering_on_off = sprintf('
				<div class="dwd-map-marker-filter-wrapper">
					<div class="dwd-map-marker-filter">
						<select class="dwd-map-select small">
							<option value="">%2$s</option>
							%1$s
						</select>
					</div>
				</div>
				',
				$marker_filtering_output_loop,
				esc_html__( 'All Categories', 'dwd-map-extended' )
			);
		}

		$marker_filtering_output = sprintf('
			%2$s
			%1$s
		',
		$marker_filtering_on_off,
			( 'off' !== $show_marker_listing ? $marker_listing_filtering_output : '' )
		);

		$output = sprintf(
			'<div%5$s class="et_pb_module et_pb_map_container_extended">
				%11$s
				%10$s
				<div class="dwd_map_extended"%8$s data-center-lat="%1$s" data-center-lng="%2$s" data-zoom="%3$d" data-gesture-handling="%7$s" data-mobile-dragging="%9$s" data-infowindow-style="%33$s" data-map-infowindow-width="%15$s" data-map-marker-animation-click="%18$s" data-map-marker-animation="%19$s" data-marker-label="%21$s" data-map-directon-text="%24$s"%12$s%13$s%14$s%16$s%17$s%20$s%28$s%29$s%32$s%35$s></div>
				%22$s
				%23$s
				<div class="dwd-map-marker-filter-row%36$s">
				%30$s
				</div>
				<table class="dwd_map_extended_child dataTable stripe%25$s%26$s%31$s%34$s" style="width:100%%">
					%4$s
				</table>
			</div>',
			esc_attr( $address_lat ),
			esc_attr( $address_lng ),
			esc_attr( $zoom_level ),
			$pins_table,
			$this->module_id(),
			$this->module_classname( $render_slug ),
			esc_attr( $gesture_handling ),
			$grayscale_filter_data,
			esc_attr( $mobile_dragging ),
			$video_background,
			$parallax_image_background,
			//12 - 31
			( '' !== $map_options ? esc_attr(" data-map-style={$map_options}") : '' ),
			esc_attr(" data-map-type={$map_type}"),
			( 'off' !== $map_custom_style ? sprintf( ' data-map-custom-style="%2$s" data-map-custom-styles="%1$s"', esc_attr( $new_dwd_replace_code_content ),
                esc_attr( $map_custom_style )
            ) : '' ),
			esc_attr( $info_window_width ),
			( 'on' === $info_window ? esc_attr(" data-info-toggle={$info_window}") : '' ),
			esc_attr(" data-map-trigger-method={$info_window_method} data-map-marker-auto-center={$marker_auto_center}"),
			esc_attr( $bounce_on_click_on_off ),
			esc_attr( $marker_animation ),
			( 'on' === $marker_clustering ? sprintf( ' data-map-marker-clustering="%1$s" data-map-marker-clustering-styles="%2$s" data-map-marker-clustering-gridsize="%4$s" data-map-marker-min-cluster-size="%5$s"%3$s',
				esc_attr( $marker_clustering ),
                esc_attr( $marker_clustering_styles ),
                ( 'on' === $marker_clustering_effect ? sprintf( ' data-marker-clustering-effect="dwd-markercluster-%1$s"',
                	esc_attr( $marker_clustering_effect_option )
            	) : '' ),
            	esc_attr( floatval($marker_cluster_gridsize) ),
            	esc_attr( floatval($marker_min_cluster_size) )
            ) : '' ),
			esc_attr( $marker_label_all ),
			( 'off' !== $map_directions && 'inside' == $map_directions_placement ? $map_direction_output : '' ),
			( 'off' !== $map_directions && 'bottom' == $map_directions_placement ? $map_direction_output : '' ),
			esc_attr( $map_directions_text ),
			( 'off' !== $map_directions ? ' dwd-map-direction' : '' ),
			( 'off' !== $show_marker_listing ? ' dwd-map-marker-listing' : '' ),
			( 'off' !== $show_marker_listing ? $marker_listing_thead_output : '' ),
			( 'on' === $show_marker_listing ? esc_attr(" data-map-marker-listing-zoom={$map_marker_listing_zoom}") : '' ),
			( 'on' === $marker_center ? esc_attr(" data-map-center={$marker_center} data-map-center-zoom={$marker_center_zoom}") : '' ),
			$marker_filtering_output,
			( 'off' !== $marker_filtering ? ' dwd-marker-category-listing' : '' ),
			sprintf( ' data-controls-ui="%1$s"%2$s',
				esc_attr( $controls_ui ),
				( 'on' === $controls_ui ? esc_attr(" data-map-type-control-style={$map_type_control_style} data-map-type-control-position={$map_type_control_position} data-map-zoom-control-position={$zoom_control_position} data-map-streetview-control-position={$streetview_control_position}") : '' )
            ),
			esc_attr( $infowindow_styles ),
			( 'off' === $show_marker_listing_icon ? ' dwd-map-marker-icon-hide' : '' ),
			( $infowindow_styles === 'custom_style' ? sprintf( ' data-infowindow-close-icon="%1$s" data-infowindow-animation-in="%2$s"',
				esc_attr(et_pb_process_font_icon($infowindow_close_icon)),
				esc_attr( $infowindow_animation_in )
			) : '' ),
			( 'on' === $show_marker_listing || 'on' === $marker_filtering ? ' dwd-map-filter-padding' : '' )
		);

		return $output;
	}
}


new ET_Builder_Module_Map_Extended;
