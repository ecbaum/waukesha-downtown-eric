<?php

/**
 * Plugin Name: Compulse WooCommerce Image Alt Attributes
 * Description: Adds alt and title attributes with product names on WooCommerce product images.
 * Author: Kevin Hall
 */

$_sbg_woocommerce_product_id = 0;

function sbg_add_woocommerce_product_image_alt_tags($attr, $attachment, $size) {
	global $_sbg_woocommerce_product_id;

	$title = get_the_title($_sbg_woocommerce_product_id);

	if (!array_key_exists("alt", $attr) || "" == $attr["alt"]) {
		$attr["alt"] = $title;
	}

	if (!array_key_exists("title", $attr) || "" == $attr["title"]) {
		$attr["title"] = $title;
	}

	remove_filter('wp_get_attachment_image_attributes', 'sbg_add_woocommerce_product_image_alt_tags', 99);

	return $attr;
}

function sbg_add_woocommerce_gallery_image_alt_tags($attr, $attachment, $size) {
	global $product;

	if (is_product() && in_array($attachment -> ID, $product -> get_gallery_image_ids())) {
		$title = get_the_title($product -> get_id());

		if (!array_key_exists("alt", $attr) || "" == $attr["alt"]) {
			$attr["alt"] = $title;
		}

		if (!array_key_exists("title", $attr) || "" == $attr["title"]) {
			$attr["title"] = $title;
		}
	}

	return $attr;
}

function sbg_add_woocommerce_product_image_filter($post_id, $image_id, $size) {
	global $_sbg_woocommerce_product_id;

	// Only add the alt tag if it's the post thumbnail for a product.
	if ("product" == get_post_type($post_id)) {
		$_sbg_woocommerce_product_id = $post_id;
		add_filter('wp_get_attachment_image_attributes', 'sbg_add_woocommerce_product_image_alt_tags', 99, 3);
	}
}

function sbg_add_alt_tag_filter() {
	// Only do this if WooCommerce is installed.
	if (function_exists("WC")) {
		add_action('begin_fetch_post_thumbnail_html', 'sbg_add_woocommerce_product_image_filter', 99, 3);
		add_action('wp_get_attachment_image_attributes', 'sbg_add_woocommerce_gallery_image_alt_tags', 99, 3);
	}
}
add_action("after_setup_theme", "sbg_add_alt_tag_filter");