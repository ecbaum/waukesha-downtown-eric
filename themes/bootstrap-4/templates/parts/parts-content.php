<?php
$id = get_the_ID();
$blocks = parse_blocks(get_the_content(null, false, $id));
if (!$blocks || $blocks[0]['blockName'] != 'acf/home-hero') {  
   get_template_part('templates/parts/parts', 'nav');
} 
if (!empty($blocks)) {
 for ($i = 0; $i < count($blocks); $i++) {
   if (substr($blocks[$i]['blockName'], 0, 3) === 'acf') {
       echo render_block( $blocks[$i] );
   } else {
     if ($blocks[$i]['blockName']) { //Stops null blocks from outputting
     echo '<div class="col-10 offset-1 guten-block">';
     echo apply_filters( 'the_content', render_block( $blocks[$i] ) );
     echo '</div>';
     }
   }
 }
} else {
  the_content( $id);
}
