<?php

require_once 'includes/blocks.php';

function compulse_enqueue_scripts() {
	$style_name = 'compulse';

	wp_register_style($style_name, get_stylesheet_directory_uri() . '/style.css');
	wp_enqueue_style($style_name);

	wp_enqueue_script('vendor', get_stylesheet_directory_uri() . '/js/vendor.js', array(), '', true);
	wp_enqueue_script('app', get_stylesheet_directory_uri() . '/js/app.js', array(), '', true);
}
add_action('wp_enqueue_scripts', 'compulse_enqueue_scripts');


register_nav_menus( array(
    'primary' => 'Primary Menu',
) );


require_once 'bs4Navwalker.php';

