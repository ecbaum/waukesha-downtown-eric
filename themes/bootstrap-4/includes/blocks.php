<?php
add_filter('block_categories', function ($categories, $post) {
  return array_merge(
    $categories,
    array(
      array(
        'slug'  => 'compulse',
        'title' => 'Compulse Blocks',
      ),
    )
  );
}, 10, 2);


/*** Blocks ***/
function register_acf_block_types() {

    acf_register_block_type(array(
      'name'              => 'home-hero',
      'title'             => __('Home Hero'),
      'description'       => __('Home Hero - first page'),
      'render_template'   => 'templates/blocks/home-hero.php',
      'category'          => 'compulse',
      'icon'              => 'admin-comments',
      'keywords'          => array( 'hero', 'home' ),
      'mode'              => 'edit',
      'supports' => array(
        'mode' => false, // Turns off preview mode permanently
        'multiple' => false, // Does not allow multiple of this block on one page
        'anchor' => false // If true, allows custom ID
      )
  ));

}


// Check if function exists and hook into setup.
if (function_exists('acf_register_block_type')) {
  add_action('acf/init', 'register_acf_block_types');
}
